package be.moon.util.person;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class PersonGenerator {
    private final static Random RAND_NUM_GEN = new Random();

    public static Person generate() {
        Gender gender = genGender();
        String voornaam = genVoornaam(gender);
        String achternaam = genAchternaam();
        LocalDate dob = genDateOfBirth();
        double gewicht = genGewicht();
        double lengte = genLengte();
        return new Person(voornaam, achternaam, gender, dob, gewicht, lengte);
    }

    public static Collection<Person> generatePeople(int amount) {
        Collection<Person> people = new TreeSet<>();
        for (int i = 0; i < amount; i++) {
            people.add(generate());
        }
        return people;
    }

    private static float genLengte() {
        final float MAX_HEIGHT = 200;
        final float MIN_HEIGHT = 160;
        float range = MAX_HEIGHT - MIN_HEIGHT;
        return MIN_HEIGHT + (RAND_NUM_GEN.nextFloat() * range);
    }

    private static float genGewicht() {
        final float MAX_WEIGHT = 100;
        final float MIN_WEIGHT = 60;
        float range = MAX_WEIGHT - MIN_WEIGHT;
        return MIN_WEIGHT + (RAND_NUM_GEN.nextFloat() * range);
    }

    private static LocalDate genDateOfBirth() {
        long minDay = LocalDate.of(1970, 1, 1).toEpochDay();
        long maxDay = LocalDate.now().toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(maxDay - minDay + 1);
        return LocalDate.ofEpochDay(randomDay);
    }

    private static String genAchternaam() {
        String[] achternaamList = geefAchternaamList();
        return (String) randElementFromArray(achternaamList);
    }

    private static String[] geefAchternaamList() {
        return new String[]{
                "Van Ham",
                "Jones",
                "Smith",
                "Warmowski",
                "Peeters",
                "Janssens",
                "Dubois",
                "Maes",
                "Martin",
                "Jacobs",
                "Claes",
                "Dupont",
                "Mertens",
                "Leroy",
                "Dumont",
                "Lefebvre",
                "Wouters",
                "Lambert",
                "Fontaine",
                "Willems",
                "Pauwels",
                "Aerts",
                "Goossens",
                "Desmet",
                "Devos",
                "Simon",
                "Leclercq",
                "Petit",
                "Bertrand",
                "Andre",
                "Lejeune",
                "Delvaux",
                "Hermans",
                "Hendrickx",
                "Thomas",
                "Lemmens",
                "Renard",
                "Vermeulen",
                "Michiels",
                "Evrard",
                "Gerard",
                "Janssen",
                "Smets",
                "Noel",
                "Charlier",
                "Lefevre",
                "Laurent",
                "Bernard",
                "Robert",
                "Francois",
                "Rousseau",
                "Lemaire",
                "Adam",
                "Lecomte",
                "Martens",
                "Carlier",
                "Dujardin",
                "Michel",
                "Collard",
                "Marchal",
                "Deprez",
                "Coppens",
                "Louis",
                "Masson",
                "Declercq",
                "Claeys",
                "Cornet",
                "Jansen",
                "Denis",
                "Legrand",
                "Antoine",
                "Baert",
                "Muller",
                "Mathieu",
                "Marechal",
                "Gregoire",
                "Verhoeven",
                "Segers",
                "Moreau",
                "Thiry",
                "Boulanger",
                "Thys",
                "Beckers",
                "Descamps",
                "Bosmans",
                "Leonard",
                "Cools",
                "Bastin",
                "Verbeke",
                "Collin",
                "Simons",
                "Parmentier",
                "Guillaume",
                "Daniels",
                "Coenen",
                "Remy",
                "Vandamme",
                "Lacroix",
                "Vincent",
                "Mahieu",
                "Cornelis",
                "Bauwens",
                "Declerck",
                "Bogaert"
        };
    }

    private static Gender genGender() {
        return (Gender) randElementFromArray(Gender.values());
    }

    private static Object randElementFromArray(Object[] array) {
        return array[randIdx(array)];
    }

    private static int randIdx(Object[] array) {
        int arrayLength = array.length;
        return RAND_NUM_GEN.nextInt(arrayLength);
    }

    private static String genVoornaam(Gender gender) {
        String[] voornaamList;
        if (gender == Gender.MALE) {
            voornaamList = geefVoornaamListMale();
        } else {
            voornaamList = geefVoornaamListFemale();
        }
        return (String) randElementFromArray(voornaamList);

    }

    private static String[] geefVoornaamListFemale() {
        return new String[]{
                "Danielle",
                "Janine",
                "Kelly",
                "Fern",
                "Nancy",
                "Martine",
                "Ariel",
                "Avena",
                "Leanne",
                "Emma",
                "Louise",
                "Elise",
                "Olivia",
                "Lina",
                "Marie",
                "Lucie",
                "Ella",
                "Alice",
                "Juliette",
                "Mila",
                "Chloe",
                "Elena",
                "Anna",
                "Camille",
                "Lea",
                "Nina",
                "Lena",
                "Charlotte",
                "Julie",
                "Noor",
                "Zoe",
                "Sofia",
                "Jade",
                "Nora",
                "Sarah",
                "Laura",
                "Eva",
                "Julia",
                "Manon",
                "Sara",
                "Lily",
                "Lotte",
                "Lisa",
                "Lore",
                "Aya",
                "Victoria",
                "Clara",
                "Fien",
                "Elisa",
                "Amelie",
                "Mia",
                "Giulia",
                "Luna",
                "Anaïs",
                "Lola",
                "Amber",
                "Lara",
                "Yasmine",
                "Ines",
                "Lou",
                "Inaya",
                "Jeanne",
                "Fleur",
                "Renee",
                "Nour",
                "Noémie",
                "Elsa",
                "Helena",
                "Ines",
                "Pauline",
                "Margaux",
                "Alicia",
                "Liv",
                "Emilie",
                "Lilou",
                "Valentina",
                "Amina",
                "Axelle",
                "Celia",
                "Enora",
                "Maryam",
                "Malak",
                "Eline",
                "Janne",
                "Alix",
                "Noa",
                "Amira",
                "Zoe",
                "Mona",
                "Hanne",
                "Jana",
                "Lise",
                "Mira",
                "Elif",
                "Emily",
                "Nore",
                "Sophia",
                "Fenna",
                "Marion",
                "Norah",
                "Lena",
                "Maria",
                "Rose",
                "Estelle",
                "Maya",
                "Clémence",
                "Kato",
                "Laure",
                "Margot"
        };
    }

    private static String[] geefVoornaamListMale() {
        return new String[]{
                "Yannick",
                "Xavier",
                "Jan",
                "Sven",
                "Jonathan",
                "Nick",
                "Brandon",
                "Aiden",
                "Benedict",
                "Louis",
                "Lucas",
                "Arthur",
                "Adam",
                "Noah",
                "Liam",
                "Mohamed",
                "Nathan",
                "Jules",
                "Mathis",
                "Victor",
                "Gabriel",
                "Hugo",
                "Vince",
                "Finn",
                "Ethan",
                "Eden",
                "Thomas",
                "Theo",
                "Matteo",
                "Maxime",
                "Rayan",
                "Oscar",
                "Alexander",
                "Seppe",
                "Stan",
                "Lars",
                "Luca",
                "Milan",
                "Simon",
                "Leon",
                "Kobe",
                "David",
                "Raphael",
                "Tom",
                "Aaron",
                "Robin",
                "Nolan",
                "Sacha",
                "Alexandre",
                "Wout",
                "Mats",
                "Elias",
                "Vic",
                "Daan",
                "Samuel",
                "Timéo",
                "Senne",
                "Yanis",
                "Mathéo",
                "Jack",
                "William",
                "Mathias",
                "Ferre",
                "Tuur",
                "Amir",
                "Warre",
                "Imran",
                "Sam",
                "Lowie",
                "Maxim",
                "Nicolas",
                "Felix",
                "Alex",
                "Mathys",
                "Emile",
                "Clement",
                "Leon",
                "Antoine",
                "Jayden",
                "Ibrahim",
                "Robbe",
                "Ruben",
                "Enzo",
                "Leo",
                "Cas",
                "Youssef",
                "Alexis",
                "Lewis",
                "Mauro",
                "Nand",
                "Diego",
                "Emiel",
                "Sem",
                "Jasper",
                "Baptiste",
                "Benjamin",
                "Hamza",
                "Lukas",
                "Guillaume",
                "Tiago",
                "Loïc",
                "Romain",
                "Alessio",
                "Martin",
                "Ali",
                "Axel",
                "Milo",
                "Ilyas",
                "Ayoub"
        };
    }
}
