package be.moon.util.person;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class Person implements Serializable {
    private String firstName;
    private String surname;
    private Gender gender;
    private LocalDate dob;
    private double mass;
    private double height;

    public Person(LocalDate dob) {
        this("Joe", "Smith", Gender.OTHER, dob, 0, 0);

    }

    public Person(String firstName, String surname, Gender gender, LocalDate dob, double mass, double height) {
        this.firstName = firstName;
        this.surname = surname;
        this.gender = gender;
        this.dob = dob;
        this.mass = mass;
        this.height = height;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public long calcAge(){
        return ChronoUnit.YEARS.between(getDob(), LocalDate.now());
    }

    @Override
    public String toString() {
        return String.format("Person{firstName='%s', surname='%s', gender=%s, dob=%s, mass=%.2fkg, height=%.2fcm}",
                getFirstName(), getSurname(), getGender(), getDob(), getMass(), getHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Double.compare(person.mass, mass) == 0 &&
                Double.compare(person.height, height) == 0 &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(surname, person.surname) &&
                gender == person.gender &&
                Objects.equals(dob, person.dob);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, surname, gender, dob, mass, height);
    }

   // @Override
    public int compareTo(Person otherPerson) {
        return this.getSurname().compareTo(otherPerson.getSurname());
    }

  /*  @Override
    public int compareTo(Object o) {
        return 0;
    }*/
}
