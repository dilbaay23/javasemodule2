package be.moon.util;

import java.util.Scanner;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public abstract class KeyboardUtility {
    private static final String INVALID_MSG = "Invalid input! Please try again...";
    public static final Scanner KEYBOARD = new Scanner(System.in);

    public static int askForInt(String message) {
        while (true) {
            String input = ask(message);
            try {
                return Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    public static boolean askYOrN(String message) {
        while (true) {
            String input = ask(message + "(y/n)");
            char firstLetter = '0';
            try {
                firstLetter = input.toLowerCase().charAt(0);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
                continue;
            }
            switch (firstLetter) {
                case 'y':
                    return true;
                case 'n':
                    return false;
                default:
                    break;
            }
        }
    }

    public static String ask(String message) {
        System.out.println(message);
        return KEYBOARD.nextLine();
    }

    public static double askForDouble(String message) {
        while (true) {
            String input = ask(message);
            try {
                return Double.parseDouble(input);
            } catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    public static int askForChoice(String message, String[] options) {
        System.out.println(message);
        return askForChoice(options);
    }

    public static int askForChoice(String[] options) {
        while (true) {
            for (int i = 0; i < options.length; i++) {
                System.out.printf("%d. %s%n", i + 1, options[i]);
            }
            int chosenIdx = askForInt(String.format("Enter your choice (1-%d):", options.length)) - 1;
            if (chosenIdx < 0 || chosenIdx >= options.length){
                System.out.println(INVALID_MSG);
                System.out.println("Please enter a choice in the valid range");
            } else {
                return chosenIdx;
            }
        }
    }
}
