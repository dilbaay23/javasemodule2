package be.moon.hs18;

/**
 * Created By Moon
 * 2/3/2021, Wed
 **/
public class TextProcessApp {
    public static void main(String[] args) {
        TextPrinter textPrinter = new TextPrinter("All those moments will be lost like tears in rain. Come here, see and read my sentence...");

        textPrinter.printProcessedWords(w -> String.format("<<%s>>", w));
        System.out.println("-----------------------");

        textPrinter.printProcessedWords(s -> TextUtil.quote(s));
        System.out.println("-----------------------");

        textPrinter.printProcessedWords(TextUtil::quote);   // This is same with second...
        System.out.println("-----------------------");


        TextPadder textPadder = new TextPadder(20);
        textPrinter.printProcessedWords(s -> textPadder.pad(s));

        System.out.println("-----------------------");

        textPrinter.printProcessedWords(textPadder::pad);

        textPrinter.printProcessedWords(String::toUpperCase);

        System.out.println("-----------------------");

        //Opdracht-2  4.puntje
        textPrinter.printProcessedWords(TextUtil::reverse);

        System.out.println("-----------------------");

        //Opdracht-2  5.puntje
        TextScrumbler textScrumbler = new TextScrumbler();
        textPrinter.printProcessedWords(textScrumbler::scramble);

        System.out.println("-----------------------");



    }
}
