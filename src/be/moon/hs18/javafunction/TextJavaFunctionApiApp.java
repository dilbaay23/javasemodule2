package be.moon.hs18.javafunction;


import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created By Moon
 * 2/2/2021, Tue
 **/
public class TextJavaFunctionApiApp {
    public static void main(String[] args) {

        Consumer<String> printer = System.out ::println;
        TextPrinter textPrinter = new TextPrinter("All those moments will be lost like tears in rain. Come here, see and read my sentence...",printer);
        Predicate<String> condition1 = s -> s.contains("a");
        Predicate<String> condition2 = s -> s.contains("e");
        Predicate<String> condition3 = condition1.and(condition2);
        textPrinter.printFilteredWords(condition3);

        System.out.println("*************************");

        textPrinter.printProcessedWords(String::toUpperCase);

        System.out.println("*************************");

        textPrinter.printProcessedAsReversedWords(String::toUpperCase);



        /*
        textPrinter.printFilteredWords((w) -> w.toLowerCase().contains("e"));

        System.out.println("*************************");
        textPrinter.printFilteredWords((w) -> w.length()>4);

        System.out.println("*************************");

        Predicate<String> condition1 = s -> s.length() == 4;
        Predicate<String> condition2 = s -> s.contains("e");
        Predicate<String> condition3 = condition1.and(condition2);
        Predicate<String> condition4 = condition1.or(condition2);
        Predicate<String> condition5 = condition2.negate();


        textPrinter.printFilteredWords(condition3);
        System.out.println("*************************");
        textPrinter.printFilteredWords(condition4);
        System.out.println("*************************");
        textPrinter.printFilteredWords(condition5);

        textPrinter.printProcessedWords(String::toUpperCase);

        TextPrinter tp = new TextPrinter("110 154 236 123");
        tp.printNumberValues(BigDecimal::new);

        System.out.println("*************************");

        Consumer<String> printer = System.out ::println;
        textPrinter.printFilteredWordsWithConsumer(s -> s.contains("t"),printer);

        System.out.println("*************************");
        tp.printProcessedWordsWithConsumer(s ->String.format("<<%s>>",s), printer);

         */




    }

}
