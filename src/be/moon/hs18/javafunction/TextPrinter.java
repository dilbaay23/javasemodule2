package be.moon.hs18.javafunction;

import be.moon.hs18.NumberParser;
import be.moon.hs18.WordFilter;
import be.moon.hs18.WordProcessor;

import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created By Moon
 * 2/2/2021, Tue
 **/
public class TextPrinter {
    private String sentence;

    public TextPrinter(String sentence,Consumer<String> print) {
        this.sentence = sentence;
    }

    public void printFilteredWords(Predicate<String> filter) {
        String[] words = sentence.split(" ");
        for (String word : words) {
            if (filter.test(word)) {
                System.out.println(word);
            }
        }
    }


    public void printProcessedWords(Function<String,String> processor) {
        String[] words = sentence.split(" ");
        for (String word : words) {
                System.out.println(processor.apply(word));
        }
    }

    public void printNumberValues(Function<String,BigDecimal> parser) {
        String[] words = sentence.split(" ");
        for (String word : words) {
            System.out.println(parser.apply(word));
        }
    }

    public void printSum(Function<String,BigDecimal> parser) {
        String[] words = sentence.split(" ");
        int sum = 0;
        for (String word : words) {
            BigDecimal parse = parser.apply(word);
            sum += parse.intValue();
        }
        System.out.println(sum);
    }

    public void printFilteredWordsWithConsumer(Predicate<String> filter, Consumer<String> printer) {
        String[] words = sentence.split(" ");
        for (String word : words) {
            if (filter.test(word)) {
                printer.accept(word);
            }
        }
    }
    public void printProcessedWordsWithConsumer(Function<String,String> processor, Consumer<String> printer) {
        String[] words = sentence.split(" ");
        for (String word : words) {
            printer.accept(processor.apply(word));
        }
    }

    public void printProcessedAsReversedWords(Function<String,String> processor) {
        String[] words = sentence.split(" ");

        for (String word : words) {
            StringBuilder sb = new StringBuilder(word);
            sb= sb.reverse();
            System.out.println(processor.apply(sb.toString()));
        }
    }
}
