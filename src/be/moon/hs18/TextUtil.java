package be.moon.hs18;

/**
 * Created By Moon
 * 2/3/2021, Wed
 **/

//Text Utility can be class instead of interface too.it is not about interface . it will work good in TextProcessApp too.
public interface TextUtil {

    static String quote(String s){
        return String.format("<<%s>>",s);
    }

    static String reverse(String s){
        StringBuilder sb = new StringBuilder(s);
        sb = sb.reverse();
        return sb.toString();
    }
}
