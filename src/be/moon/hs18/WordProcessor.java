package be.moon.hs18;

/**
 * Created By Moon
 * 2/3/2021, Wed
 **/

@FunctionalInterface
public interface WordProcessor {
    String process(String word);
}
