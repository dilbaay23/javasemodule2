package be.moon.hs18;

import java.math.BigDecimal;
import java.util.function.Function;

/**
 * Created By Moon
 * 2/3/2021, Wed
 **/
public class TextParserApp {
    public static void main(String[] args) {
        TextPrinter tp = new TextPrinter("110 154 236 123");

      //Opdracht-2  9.puntje

        tp.printNumberValues(s -> new BigDecimal(s));


        System.out.println("***********************");

        tp.printNumberValues(BigDecimal::new); // same as first

        //Opdracht-2  10.puntje

        tp.printSum(BigDecimal::new);

        System.out.println("-----------------------");




    }
}
