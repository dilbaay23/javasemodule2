package be.moon.hs18;

/**
 * Created By Moon
 * 2/2/2021, Tue
 **/
@FunctionalInterface
public interface WordFilter {
    boolean isValid(String word);
}
