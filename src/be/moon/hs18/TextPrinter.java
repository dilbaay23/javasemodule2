package be.moon.hs18;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Created By Moon
 * 2/2/2021, Tue
 **/
public class TextPrinter {
    private String sentence;

    public TextPrinter(String sentence) {
        this.sentence = sentence;
    }

    public void printFilteredWords(WordFilter filter) {
        String[] words = sentence.split(" ");
        for (String word : words) {
            if (filter.isValid(word)) {
                System.out.println(word);
            }
        }
    }

    public void printProcessedWords(WordProcessor processor) {
        String[] words = sentence.split(" ");
        for (String word : words) {
                System.out.println(processor.process(word));
        }
    }

    public void printNumberValues(NumberParser parser) {
        String[] words = sentence.split(" ");
        for (String word : words) {
            System.out.println(parser.parse(word));
        }
    }

    public void printSum(NumberParser parser) {
        String[] words = sentence.split(" ");
        int sum = 0;
        for (String word : words) {
            BigDecimal parse = parser.parse(word);
            sum += parse.intValue();
        }
        System.out.println(sum);
    }
}
