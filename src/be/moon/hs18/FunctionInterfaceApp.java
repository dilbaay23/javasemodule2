package be.moon.hs18;

import java.util.function.Function;

/**
 * Created By Moon
 * 2/3/2021, Wed
 **/
public class FunctionInterfaceApp {
    public static void main(String[] args) {
        String text = "  Moon Is A Java Developer  ";
        String text2 = "I love Java ";

        Function<String,String > toUpperCase = String::toUpperCase;
        text= toUpperCase.apply(text);
        System.out.println(text);

        Function<String,String > toTrimmedText = String::trim;
        text= toTrimmedText.apply(text);
        System.out.println(text);

        Function<String,String > concattedText =text2 ::concat;
        text= concattedText.apply(text);
        System.out.println(text);

        Function<String,String > trimAndThenToUpperCaseAndThenConcat = toTrimmedText.andThen(concattedText).andThen(toUpperCase);
        text = trimAndThenToUpperCaseAndThenConcat.apply(text);
        System.out.println(text);
    }
}
