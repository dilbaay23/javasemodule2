package be.moon.hs18;

import java.math.BigDecimal;

/**
 * Created By Moon
 * 2/3/2021, Wed
 **/
@FunctionalInterface
public interface NumberParser {
     BigDecimal parse(String s);

}
