package be.moon.hs18;

/**
 * Created By Moon
 * 2/2/2021, Tue
 **/
public class TextFilterApp {
    public static void main(String[] args) {

        TextPrinter textPrinter = new TextPrinter("All those moments will be lost like tears in rain. Come here, see and read my sentence...");

        //First Option with an anonymous class
        WordFilter filter = new WordFilter(){
            @Override
            public boolean isValid(String word){
                if (word.contains("ll")){
                    return true;
                } else {
                    return false;
                }
            }
        };

      textPrinter.printFilteredWords(filter);

        System.out.println("*************************");

        //Second Option with an implementation direct as a parameter

        textPrinter.printFilteredWords(
                new WordFilter() {
                    @Override
                    public boolean isValid(String word) {
                        if (word.toLowerCase().contains("a")){
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
        );

        System.out.println("*************************");

        //Third Option is a shorter way of second option --- some unnecessary( template) codes are cleaned

        textPrinter.printFilteredWords((String word) -> {
            if (word.toLowerCase().contains("e")){
                return true;
            } else {
                return false;
            }
        });

        System.out.println("*************************");

        //Forth Option is a shorter way of third option --- some more unnecessary codes are cleaned
        textPrinter.printFilteredWords((String word) -> {
            return word.toLowerCase().contains("i");
        });

        System.out.println("*************************");

        //Fifth Option is a shorter way of forth option --- some more unnecessary codes are cleaned
        textPrinter.printFilteredWords((String word) -> word.toLowerCase().contains("o"));

        System.out.println("*************************");

        //Shortest way of second way....
        textPrinter.printFilteredWords((w) -> w.toLowerCase().contains("e"));

        System.out.println("*************************");
        textPrinter.printFilteredWords((w) -> w.length()>4);

        System.out.println("*************************");
        textPrinter.printFilteredWords((w) -> w.toLowerCase().charAt(0)=='a');

        System.out.println("*************************");
        textPrinter.printFilteredWords((w) -> w.toLowerCase().charAt(1)=='e');

        System.out.println("*************************");
        textPrinter.printFilteredWords((w) -> {
            int count = 0;
                    for (int i = 0; i < w.length(); i++) {
                        if (w.charAt(i) == 'e') {
                            count++;
                        }
                    }
            if(count == 2) {
                return true;
            }
            return false;
        });

    }

}
