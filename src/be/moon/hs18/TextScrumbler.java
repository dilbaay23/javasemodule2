package be.moon.hs18;

import java.util.Locale;

/**
 * Created By Moon
 * 2/3/2021, Wed
 **/
public class TextScrumbler {

    public  String scramble(String wordToScramble){
        String scrumbledWord = wordToScramble.toLowerCase();
        String[][] replaceableLetters = new String[][]{
                {"a", "@"},
                {"e", "€"},
                {"l", "1"},
                {"o", "0"}

        };
        for (int i = 0; i <replaceableLetters.length ; i++) {
            scrumbledWord = scrumbledWord.replaceAll(replaceableLetters[i][0],replaceableLetters[i][1]);
        }
        return scrumbledWord;
    }
}
