package be.moon.hs17.genericmethods;

import java.util.Random;

public class GenericsMethodsApp {

        private static final Random RAND_NUM_GEN = new Random();

        public static void main(String[] args) {

            String[] names = {
                    "Elif",
                    "Baha",
                    "Yunus",
                    "Betul",
                    "Hamid",
                    "Mehmet Akif",
                    "Latif",
                    "Nilufer",
                    "Asuman",
                    };

            System.out.printf("The last name is %s%n", names[names.length-1]);

            String randomName = getRandomElementFromArray(names);
            System.out.printf("Picking out a random name ... the winner is ... %s!%n", randomName);

            Car[] myCars = new Car[]{
                    new Car("yellow", "Lamborghini"),
                    new Car("white", "BMW"),
                    new Car("black", "Porsche"),
                    new Car("red", "Ferrari")
            };

            Car myCarForToday = getRandomElementFromArray(myCars);

            System.out.printf("Deciding which car to drive today... it's gonna be my %s%n", myCarForToday);

            Boat[] port = new Boat[]{
                    new Boat("black", "Yamaha"),
                    new Boat("white", "Harley Davidson"),
                    new Boat("red", "Ducati")
            };

            Boat boatToSearch = getRandomElementFromArray(port);
            System.out.printf("Searching random boat in port... the %s%n", boatToSearch);

            Integer[] primeNumbers = {
                    2,
                    3,
                    5,
                    7,
                    11
            };

            Integer randomPrimeNumber = getRandomElementFromArray(primeNumbers);
            System.out.printf("A random prime number...%d%n", randomPrimeNumber);

            HogwartsHouse[] houses = HogwartsHouse.values();
            HogwartsHouse myChosenHouse = getRandomElementFromArray(houses);
            System.out.printf("Sorting Hat:\"Hmm....yes...you are definitely a...%s\"%n", myChosenHouse);

        }
        private static <T> T getRandomElementFromArray(T[] elements){
            int randIdx = RAND_NUM_GEN.nextInt(elements.length);
            return elements[randIdx];
        }

       /* private static Object getRandomObjectFromArray(Object[] objects){
            int randIdx = RAND_NUM_GEN.nextInt(objects.length);
            return objects[randIdx];
        }
*/
/*        private static Vehicle getRandomVehicleFromArray(Vehicle[] vehicles) {
            int randIdx = RAND_NUM_GEN.nextInt(vehicles.length);
            return vehicles[randIdx];
        }

        private static Boat getRandomBoatFromArray(Boat[] boats) {
            int randIdx = RAND_NUM_GEN.nextInt(boats.length);
            return boats[randIdx];
        }

        private static Car getRandomCarFromArray(Car[] cars) {
            int randIdx = RAND_NUM_GEN.nextInt(cars.length);
            return cars[randIdx];
        }

        private static String getRandomStringFromArray(String[] strings) {
            int randIdx = RAND_NUM_GEN.nextInt(strings.length);
            return strings[randIdx];
        }*/




}
