package be.moon.hs17.genericmethods;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class Boat extends Vehicle{
    private String anchor;

    public Boat(String colour, String make) {
        super(colour, make);
    }

    @Override
    public void accelerate() {
        System.out.println("The wind is on our side");
    }

    @Override
    public void slowDown() {
        System.out.println("Slowing down the boat...");
    }

    public void dropAnchor(){
        System.out.println("Dropping anchor...");
        slowDown();
    }

}
