package be.moon.hs17.genericmethods;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class Car extends Vehicle{
    public Car(String colour, String make){
        super(colour, make);
    }

    @Override
    public void accelerate() {
        System.out.println("Vroom vroom");
    }

    @Override
    public void slowDown() {
        System.out.println("*brake noises*");
    }

}
