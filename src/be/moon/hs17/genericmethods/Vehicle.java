package be.moon.hs17.genericmethods;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public abstract class Vehicle {
    private String colour;
    private String make;
    private String model;
    private double topSpeed;

    public Vehicle(String colour, String make) {
        this.colour = colour;
        this.make = make;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(double topSpeed) {
        this.topSpeed = topSpeed;
    }

    @Override
    public String toString() {
        return getColour() + " " + getMake();
    }

    public abstract void accelerate();

    public abstract void slowDown();
}
