package be.moon.hs17.genericslass;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public interface GenericDatabase<T,K> {
    void save(T entity);

    void delete(K id);

    T findById(K id);

    T[] retrieveAll();
}
