package be.moon.hs17.genericslass;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class GenericDatabaseImpl<T,K> implements GenericDatabase<T,K>{

    @Override
    public void save(T entity){
        System.out.printf("Saving %s.... Saved!%n", entity.getClass().getSimpleName());
    }

    @Override
    public void delete(K id){
        System.out.printf("Deleting Entity with id %d%n", id);
    }

    @Override
    public T findById(K id ){
        System.out.printf("Retrieving entity with id %d%n....", id);
        return null;
    }

    @Override
    public T[] retrieveAll(){
        System.out.println("Retrieving all entites");
        return null;
    }
}
