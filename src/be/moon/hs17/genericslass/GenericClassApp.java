package be.moon.hs17.genericslass;

import be.moon.hs17.genericmethods.Boat;
import be.moon.hs17.genericmethods.Car;
import be.moon.hs17.genericmethods.Vehicle;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class GenericClassApp {

    public static void main(String[] args) {

        ObjectDatabase myObjectDatabase = new ObjectDatabase();
        User moon = new User();
        Product laptop = new Product();

        myObjectDatabase.save(moon);
        myObjectDatabase.save(laptop);
        User loggedInUser = (User) myObjectDatabase.findById(1257692579);
        Product[] products = (Product[]) myObjectDatabase.retrieveAll();
        System.out.println("------------------------");


        GenericDatabaseImpl<Product,Long> myGenericProductDb = new GenericDatabaseImpl();
        myGenericProductDb.save(laptop);
        myGenericProductDb.delete(555L);
        myGenericProductDb.retrieveAll();
        myGenericProductDb.findById(555L);
        System.out.println("------------------------");

        GenericDatabaseImpl<User,Long> myGenericUserDb = new GenericDatabaseImpl();
        myGenericUserDb.save(moon);
        myGenericUserDb.delete(3333L);
        myGenericUserDb.retrieveAll();
        myGenericUserDb.findById(3333L);
        System.out.println("------------------------");

        GenericVehicleDatabaseImpl<Vehicle, Long> myVehicleDB = new GenericVehicleDatabaseImpl<>();
        myVehicleDB.save(new Car("yellow", "Lamborghini"));
        myVehicleDB.save(new Boat("green", "Yamaha"));


    }
}
