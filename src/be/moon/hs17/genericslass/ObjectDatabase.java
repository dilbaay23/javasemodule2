package be.moon.hs17.genericslass;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class ObjectDatabase {

    public void save(Object entity){
        System.out.printf("Saving %s.... Saved!%n", entity.getClass().getSimpleName());
    }

    public void delete(long id){
        System.out.printf("Deleting Entity with id %d%n", id);
    }

    public Object findById(long id ){
        System.out.printf("Retrieving entity with id %d%n....", id);
        return null;
    }

    public Object[] retrieveAll(){
        System.out.println("Retrieving all entites");
        return null;
    }
}
