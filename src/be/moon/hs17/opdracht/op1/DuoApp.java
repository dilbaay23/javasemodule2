package be.moon.hs17.opdracht.op1;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class DuoApp {
    public static void main(String[] args) {
        Duo<String> myString = new Duo<>("red", "black");
        Duo<Integer> myInt = new Duo<>(11, 22);

        System.out.printf("String duo: %s --- %s%n", myString.getFirst(), myString.getSecond());
        myString.swap();
        System.out.println("Swapped!");
        System.out.printf("String duo: %s --- %s%n", myString.getFirst(), myString.getSecond());

        System.out.println("*********************");

        System.out.printf("Integer duo: %d --- %s%n", myInt.getFirst(), myInt.getSecond());
        myInt.swap();
        System.out.println("Swapped!");
        System.out.printf("Integer duo: %d --- %d%n", myInt.getFirst(), myInt.getSecond());


    }
}
