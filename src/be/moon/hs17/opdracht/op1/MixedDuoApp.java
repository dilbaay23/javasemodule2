package be.moon.hs17.opdracht.op1;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class MixedDuoApp {
    public static void main(String[] args) {

        MixedDuo<String, Integer> mixedDuo = new MixedDuo<>("Moon", 23);

        System.out.printf("Mixed duo: %s --- %d%n", mixedDuo.getFirst(), mixedDuo.getSecond());

    }
}
