package be.moon.hs17.opdracht.op1;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class Duo<T> {
    private T first;
    private T second;

    public Duo(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public T getSecond() {
        return second;
    }

    public void setSecond(T second) {
        this.second = second;
    }
    public void swap(){
        T temp = second;
        second = first;
        first = temp;
    }
}
