package be.moon.hs17.opdracht.op1;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class MixedDuo <T,K> {
    private T first;
    private K second;

    public MixedDuo(T first, K second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public K getSecond() {
        return second;
    }

    public void setSecond(K second) {
        this.second = second;
    }

}
