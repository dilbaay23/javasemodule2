package be.moon.hs17.opdracht.op2;

/**
 * Created By Moon
 * 2/1/2021, Mon
 **/
public class NumberDuoApp {
    public static void main(String[] args) {

        NumberDuo<Number> numberDuo1 = new NumberDuo<>(5.2, 12);

        System.out.printf("Number duo1: (%s, %s) Sum: %s%n", numberDuo1.getFirst(), numberDuo1.getSecond(),
                numberDuo1.getSum());

        NumberDuo<?> numberDuo2 = new NumberDuo<Integer>(6, 8);
        // Integer i = numberDuo2.getFirst();   // compile time error
        Number i = numberDuo2.getFirst();
        //numberDuo2.setSecond(5); ----> compile time error . ? waits Number. we can not use the methods which wait a specific type parameter from us . only we can call the methods which return us a Number.

        System.out.printf("Number duo2: (%s, %s) Sum: %s%n", numberDuo2.getFirst(), numberDuo2.getSecond(),
                numberDuo2.getSum());


    }
}
