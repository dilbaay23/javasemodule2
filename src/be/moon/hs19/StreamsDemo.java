package be.moon.hs19;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created By Moon
 * 2/5/2021, Fri
 **/
public class StreamsDemo {
    public static void main(String[] args) {
        String text= "Mijn ervaring is dat het inzetten van kernkwaliteiten en het kernkwadrant, zeker in het begin van" +
                " een coachtraject of tijdens een training of teambuilding event, veel inzicht in gedrag, patronen en " +
                "kwaliteiten geeft en bovendien niet te “zweverig” overkomt. Het is dan ook in een zakelijk omgeving " +
                "uitstekend te gebruiken.";

        String[] words = text.split(" ");

        Stream<String> stream = Stream.of(words);

        long startTime1 = System.nanoTime();
        stream.filter(w ->w.contains("e"))
                .filter(w -> w.length()>4)
                .forEach(System.out::println);
        long endTime1 = System.nanoTime();
        long duration1 = endTime1 - startTime1;


        Stream<String> stream2 = Stream.of(words);
        long startTime2 = System.nanoTime();
        stream2.filter(w ->w.contains("e") &&  w.length()>4)
                .forEach(System.out::println);
        long endTime2 = System.nanoTime();
        long duration2 = endTime2- startTime2;

        System.out.println("first  : " + duration1);
        System.out.println("second : " + duration2);








    }
}
