package be.moon.hs19.opdrachten;

import be.moon.hs19.TextUtil;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static be.moon.hs19.opdrachten.Gender.*;

/**
 * Created By Moon
 * 2/5/2021, Fri
 **/
public class StreamsApp {
    public static void main(String[] args) {
        final int LOWER_LIMIT = 1;
        final int UPPER_LIMIT = 100;
        final long LOWER_LIMIT1 = 1;
        final long UPPER_LIMIT1 = 20;

        String text = "Mijn ervaring is dat het inzetten van kernkwaliteiten en het kernkwadrant, zeker in het begin van" +
                " een coachtraject of tijdens een training of teambuilding event, veel inzicht in gedrag, patronen en " +
                "kwaliteiten geeft en bovendien niet te “zweverig” overkomt. ";

        String[] words = text.split(" ");

        Stream<String> wordStream = Stream.of(words);
        wordStream.forEach(System.out::println);

        IntStream intStream = IntStream.range(0, 100);
        intStream.forEach(System.out::println);




        //Opdracht 2
        int sum = IntStream.range(LOWER_LIMIT,UPPER_LIMIT)
                .sum();
        System.out.printf("The sum of all the numbers between %d - %d is %d\n",LOWER_LIMIT, UPPER_LIMIT, sum);

        int max = IntStream.range(LOWER_LIMIT,UPPER_LIMIT)
                .max().getAsInt();
        System.out.printf("The max value between the numbers %d - %d is %d\n",LOWER_LIMIT, UPPER_LIMIT, max);

        int min = IntStream.range(LOWER_LIMIT,UPPER_LIMIT)
                .min().getAsInt();
        System.out.printf("The min value between the numbers  %d - %d is %d\n",LOWER_LIMIT, UPPER_LIMIT, min);

        double avg = IntStream.range(LOWER_LIMIT,UPPER_LIMIT)
                .average().getAsDouble();
        System.out.printf("The avg value between the numbers  %d - %d is %f\n",LOWER_LIMIT, UPPER_LIMIT, avg);

        long reduceResult = LongStream.rangeClosed(1,UPPER_LIMIT1)
                .reduce(1,(long getal,long el )->  getal * el);
        System.out.printf("After the reduce methode value between the numbers  %d - %d is %d\n",
                LOWER_LIMIT, UPPER_LIMIT, reduceResult);


        //opdracht 2 punt 2

        String result = Stream.of(words)
                .reduce((a ,b ) -> a += ";" + b).get();
        System.out.println(result);


        //opdracht 3 - 4

        Person[] people = {new Person("Moon", "KOC", FEMALE, LocalDate.of(1980, 11, 5), 60, 162),
                new Person("Star", "AS", FEMALE, LocalDate.of(1985, 10, 5), 70, 170),
                new Person("World", "DEF", FEMALE, LocalDate.of(1990, 1, 5), 50, 162),
                new Person("Mars", "ZED", MALE, LocalDate.of(1970, 3, 5), 80, 170),
                new Person("Jupiter", "ZAS", FEMALE, LocalDate.of(1975, 5, 2), 50, 1642)};

        TextUtil.printTitle("Person array demo : women list");
        Stream<Person> peopleStream = Stream.of(people);
        Person[] women = peopleStream
                .filter(p -> p.getGender() == FEMALE)
                .toArray(Person[]::new);
        Stream.of(women).forEach(System.out::println);


        TextUtil.printTitle("Person array demo : age > 35");
        Stream<Person> peopleStream2 = Stream.of(people);
        Person[] newPeople = (Person[]) peopleStream2.filter(p -> p.calculateAge() > 35)
                .toArray(Person[]::new);
        Stream.of(newPeople).forEach(System.out::println);

        TextUtil.printTitle("Person array demo : mass is more than 50 and age is more than 35 ");
        Stream.of(people)
                .filter(p -> p.getMass() > 50)
                .filter(p -> p.calculateAge() > 35)
                .forEach(System.out::println);

        TextUtil.printTitle("Person array demo : mass is more than 71 and less than 49 ");
        Stream.of(people)
                .filter(p -> p.getMass() < 71)
                .filter(p -> p.getMass() > 49)
                .forEach(System.out::println);

        TextUtil.printTitle("Person array demo : ages one by one ");
        Stream.of(people)
                .forEach(p  -> System.out.println(p.calculateAge()));


        TextUtil.printTitle("Person array demo : name + surname ");
        Stream.of(people)
                .map(person -> person.getName()+ " " +person.getSurname())
                .forEach(System.out::println);

        TextUtil.printTitle("Person array demo : average of ages");
        double ageAvg = Stream.of(people)
                .mapToDouble(Person::calculateAge)
                .average().getAsDouble();
        System.out.println(ageAvg);

        TextUtil.printTitle("Person array demo : max of ages");
        int maxAge = Stream.of(people)
                .mapToInt(p -> (int) p.calculateAge())
                .max().getAsInt();
        System.out.println(maxAge);


        TextUtil.printTitle("Person array demo : min of ages");
        int minAge= Stream.of(people)
                .mapToInt(p -> (int) p.calculateAge())
                .min().getAsInt();
        System.out.println(minAge);

        TextUtil.printTitle("Person array demo : mass one by one ");
        Stream.of(people)
                .forEach(p  -> System.out.println(p.getMass()));

        TextUtil.printTitle("Person array demo : average of mass");
        double massAvg = Stream.of(people)
                .mapToDouble(Person::getMass)
                .average().getAsDouble();
        System.out.println(massAvg);

        TextUtil.printTitle("Person array demo : max of ages");
        int maxMass = Stream.of(people)
                .mapToInt(p -> (int) p.getMass())
                .max().getAsInt();
        System.out.println(maxMass);


        TextUtil.printTitle("Person array demo : min of ages");
        int minMass= Stream.of(people)
                .mapToInt(p -> (int) p.getMass())
                .min().getAsInt();
        System.out.println(minMass);


        System.out.println("***********************************");

     /*   TextUtil.printTitle("Mapping demo : lengths of words");
        Stream.of(words)
                .mapToInt(String::length)
                .forEach(System.out::println);


        TextUtil.printTitle("Mapping demo : mapping words to UPPERCASE");
       words = Stream.of(words)
                .map(String::toLowerCase)
                .toArray(String[]::new);

        Stream.of(words)
                .map(String::toUpperCase)
                .forEach(System.out::println);*/


    }
}
