package be.moon.hs19.opdrachten;

/**
 * Created By Moon
 * 2/5/2021, Fri
 **/
public enum Gender {
    FEMALE, MALE
}
