package be.moon.hs19.opdrachten;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Created By Moon
 * 2/5/2021, Fri
 **/
public class Person {
    private String name;
    private String surname;
    private Gender gender;
    private LocalDate dob;
    private double mass;
    private double height;


    public Person(String name, String surname, Gender gender, LocalDate dob, double mass, double height) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.dob = dob;
        this.mass = mass;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public long calculateAge(){
        return ChronoUnit.YEARS.between(dob,LocalDate.now());
    }



    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", gender=" + gender +
                ", age=" + calculateAge() +
                ", mass=" + mass +
                ", height=" + height +
                '}';
    }
}
