package be.moon.javagevorderd.bundel.inout;


import be.moon.javagevorderd.bundel.inout.service.SettingsService;
import be.moon.javagevorderd.bundel.inout.service.impl.SettingsServiceImpl;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public class FlopMainApp {
    public static void main(String[] args) {
        SettingsService settingsService = new SettingsServiceImpl();
        settingsService.homePage();
    }
}
