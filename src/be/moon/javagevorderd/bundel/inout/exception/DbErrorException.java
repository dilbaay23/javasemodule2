package be.moon.javagevorderd.bundel.inout.exception;

/**
 * Created By Moon
 * 2/13/2021, Sat
 **/
public class DbErrorException extends Exception {
    public static final String MESSAGE =" There is something wrong. Please Try Again later. DB ERROR\n";
    public DbErrorException(){
        super(MESSAGE);
    }
}
