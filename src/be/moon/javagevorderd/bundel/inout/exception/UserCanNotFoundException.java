package be.moon.javagevorderd.bundel.inout.exception;

/**
 * Created By Moon
 * 2/13/2021, Sat
 **/
public class UserCanNotFoundException extends RuntimeException {
    public static final String MESSAGE =" User can not found! \n";
    public UserCanNotFoundException(){
        super(MESSAGE);
    }}
