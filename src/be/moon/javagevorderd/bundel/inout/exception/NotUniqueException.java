package be.moon.javagevorderd.bundel.inout.exception;

public class NotUniqueException extends IllegalArgumentException {
    public static final String MESSAGE =" This username is already exist! \n";
    public NotUniqueException(){
        super(MESSAGE);
    }
}
