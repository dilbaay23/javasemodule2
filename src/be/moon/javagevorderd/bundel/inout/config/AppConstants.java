package be.moon.javagevorderd.bundel.inout.config;

import java.nio.file.Path;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public class AppConstants {
    public static final Path PATH_TO_DESKTOP = Path.of(System.getProperty("user.home"), "Masaüstü");

    public static final Path PATH_TO_FLOP = Path.of("FLOP");
    public static final Path PATH_TO_USERS = Path.of("USERS");
    public static final Path PATH_TO_FFBACKUP = Path.of("FF-BACKUP");

    public static final Path ABSOLUTE_PATH_TO_FLOP = PATH_TO_DESKTOP.resolve(PATH_TO_FLOP);
    public static final Path ABSOLUTE_PATH_TO_FFBACKUP = PATH_TO_DESKTOP.resolve(PATH_TO_FFBACKUP);
    public static final Path ABSOLUTE_PATH_TO_USERS = ABSOLUTE_PATH_TO_FLOP.resolve(PATH_TO_USERS);

    public static final String PROJECT_NAME = "FLOP Finance";

    public static final String WANT_TO_CONTINUE = "Do you want to continue? ";
    public final static String DELETE_ACCOUNT = "Delete My Account";
    public final static String DELETE_ALL_ACCOUNTS = "Delete All Accounts in Database";
    public final static String QUIT = "Quit";

}
