package be.moon.javagevorderd.bundel.inout.dao;

import be.moon.javagevorderd.bundel.inout.entitty.User;

import java.util.List;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public interface UserDao {
    List<User> getUsers();
    boolean addUser(User user);
    int deleteLoggedUser(String username);
    int deleteAllUsers();
    boolean checkUserForLogin(String username, String password);
    boolean isExistUsername(String username);
}
