package be.moon.javagevorderd.bundel.inout.dao.impl;

import be.moon.javagevorderd.bundel.inout.dao.UserDao;
import be.moon.javagevorderd.bundel.inout.entitty.User;


import java.util.ArrayList;
import java.util.List;

import static be.moon.javagevorderd.bundel.inout.utility.KeyboardUtility.*;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public class UserDaoImpl implements UserDao {
    private static  List<User> users = new ArrayList<>();

    static
    {
        users.add(new User("admin","Moon", "Koc","password")) ;
    }


    public UserDaoImpl() {
    }


    public List<User> getUsers() {
        return users;
    }

    public boolean addUser(User user){
        users.add(user);
        return true;
    }

    public int deleteLoggedUser(String deletedUsername){
        int status = 0;
        for (int i = 0; i < users.size(); i++) {

            if(deletedUsername.equals(users.get(i).getUsername())){
                users.remove(users.get(i));
                status = 1;
            }
        }
       return status;
    }

    public int deleteAllUsers(){
        int status = 0;
        boolean confirmedRemove = askYorN("Do you really want to remove all Users from Database?");
        if(confirmedRemove){
            users.clear();
            status = 1;
        }
        return status;
    }

    @Override
    public boolean checkUserForLogin(String username, String password) {

        for (int i = 0; i < users.size(); i++) {
            if(username.equals(users.get(i).getUsername()) && password.equals(users.get(i).getPassword())){
              return true;
            }
        }
        return false;
    }

    @Override
    public boolean isExistUsername(String username) {

        for (int i = 0; i < users.size(); i++) {
            if(username.equals(users.get(i).getUsername())){
                System.out.println("This username is already exist! ");
                return true;
            }
        }
        return false;
    }
}
