package be.moon.javagevorderd.bundel.inout.service;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public interface SettingsService {
    void writeProperties();
    void readProperties();
    void homePage();

}
