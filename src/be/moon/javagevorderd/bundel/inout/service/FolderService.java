package be.moon.javagevorderd.bundel.inout.service;

import be.moon.javagevorderd.bundel.inout.entitty.User;

import java.io.File;
import java.util.HashMap;

/**
 * Created By Moon
 * 2/13/2021, Sat
 **/
public interface FolderService {
    void createFlopMap();

    void createUsersMap();

    void createFFBackupMap();

    HashMap<String, User> listFilesForFolder(File file);
}
