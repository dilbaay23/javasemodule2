package be.moon.javagevorderd.bundel.inout.service;

import be.moon.javagevorderd.bundel.inout.entitty.User;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public interface UserService {
    void start();
    void loginUser();
    void registerUser();
    void addUserFromFileToDaoUserList(User user);

}
