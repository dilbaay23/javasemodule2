package be.moon.javagevorderd.bundel.inout.service;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public interface QuitInterface {
    void quit();
}
