package be.moon.javagevorderd.bundel.inout.service.impl;

import be.moon.javagevorderd.bundel.inout.entitty.User;
import be.moon.javagevorderd.bundel.inout.service.FolderService;
import be.moon.javagevorderd.bundel.inout.service.SettingsService;
import be.moon.javagevorderd.bundel.inout.service.UserService;

import java.io.*;
import java.util.*;

import static be.moon.javagevorderd.bundel.inout.config.AppConstants.*;
import static be.moon.javagevorderd.bundel.inout.utility.KeyboardUtility.*;
import static be.moon.javagevorderd.bundel.inout.utility.MenuUtility.*;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public class SettingsServiceImpl implements SettingsService {

    private final UserService userService = new UserServiceImpl();
    private final FolderService folderService = new FolderServiceImpl();

    @Override
    public void homePage() {
        System.out.println(tildaLine());
        System.out.println(tildaLine());
        System.out.println(center(PROJECT_NAME));
        System.out.println(center("*** WELCOME :) ***"));
        System.out.println(tildaLine());
        System.out.println(center("*** IT IS NICE TO SEE YOU ***"));
        System.out.println(tildaLine());
        System.out.println(tildaLine());
        workFlow();

    }
    public void workFlow(){
        folderService.createFlopMap();
        folderService.createUsersMap();
        File file = new File(ABSOLUTE_PATH_TO_FLOP + "\\Users");
        saveUsersToDatabaseFromFile(folderService.listFilesForFolder(file));
        writeProperties();

    }

    private void saveUsersToDatabaseFromFile(HashMap<String, User> userHashMap) {
        for(Map.Entry<String, User> userFileName : userHashMap.entrySet())
        {
           // System.out.println(userFileName.getKey() + " " + userFileName.getValue());
            User user= (userFileName.getValue());
            userService.addUserFromFileToDaoUserList(user);

        }
    }

    @Override
    public void writeProperties() {
        Properties settings = new Properties();
        settings.setProperty("language1","NEDERLANDS");
        settings.setProperty("language2","ENGLISH");

        try (
                FileOutputStream fileOutput = new FileOutputStream(ABSOLUTE_PATH_TO_FLOP.resolve("Settings.ini").toFile())
        ) {
            settings.store(fileOutput, "");
        } catch (
                IOException ioException) {
            ioException.printStackTrace();
        }
        readProperties();

    }

    @Override
    public void readProperties() {

        Properties inputProperties = new Properties();
        try (
                FileInputStream fileInput = new FileInputStream(ABSOLUTE_PATH_TO_FLOP.resolve("Settings.ini").toFile())
        ) {
            inputProperties.load(fileInput);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        String nederlands = inputProperties.getProperty("language1");
        String english = inputProperties.getProperty("language2");

        String[] languages = {nederlands, english};
        int idLanguage = askForChoice("Please choose a language to continue", languages);
        if (idLanguage == 1) {
            userService.start();

        }else{
            //TODO : if I  have time, will write in Nederlands too...
            userService.start();
        }
    }
}
