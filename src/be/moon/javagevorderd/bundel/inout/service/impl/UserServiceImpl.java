package be.moon.javagevorderd.bundel.inout.service.impl;

import be.moon.javagevorderd.bundel.inout.dao.UserDao;
import be.moon.javagevorderd.bundel.inout.dao.impl.UserDaoImpl;
import be.moon.javagevorderd.bundel.inout.entitty.User;
import be.moon.javagevorderd.bundel.inout.exception.UserCanNotFoundException;
import be.moon.javagevorderd.bundel.inout.service.FolderService;
import be.moon.javagevorderd.bundel.inout.service.QuitInterface;
import be.moon.javagevorderd.bundel.inout.service.UserService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import static be.moon.javagevorderd.bundel.inout.utility.MenuUtility.*;
import static be.moon.javagevorderd.bundel.inout.config.AppConstants.*;
import static be.moon.javagevorderd.bundel.inout.utility.KeyboardUtility.*;


/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public class UserServiceImpl implements UserService {

    private final UserDao userDao = new UserDaoImpl();

    private final FolderService folderService = new FolderServiceImpl();

    private final QuitInterface quitInterface = new QuitInterfaceImpl();

    @Override
    public void start() {
        System.out.println(thickLine());
        System.out.println(center(PROJECT_NAME));
        System.out.println(thickLine());
        boolean haveAccount = askYorN("Do you want to continue with an exist Account ?") ;

        if (haveAccount) {
            loginUser();
        }
        else {
            registerUser();
        }
    }

    @Override
    public void loginUser() {
        System.out.println(thickLine());
        System.out.println(thinLine());
        System.out.println(center("Welcome to LOGIN PAGE"));
        System.out.println(thinLine());
        System.out.println(thickLine());
        String username = ask("Enter your username : ");
        String password = ask("Enter your password: ");

        if(userDao.checkUserForLogin (username,password)){
            System.out.println("Successful Login! ! ");
            menu(username);
        }else {
            System.out.println("Username or/and Password not correct ! ");
            if(askYorN(WANT_TO_CONTINUE)){
                loginUser();
            }

            else{
                quitInterface.quit();
            }
        }
    }

    private void menu(String username) {
        String[] menuActions = {DELETE_ACCOUNT, DELETE_ALL_ACCOUNTS, QUIT};
        int chosenIndex =askForChoice("Choose your action please.", menuActions);
        callAction(username,chosenIndex);
    }

    private void callAction(String username, int chosenIndex) {
        switch (chosenIndex){
            case 0 :
                deleteLoggedUser(username);
                System.out.println(thickLine());
                System.out.println("Your account is deleted successfully. You are logged out... ");
                System.out.println(thickLine());
                askLoginOrNewRegisterOrQuit();
                break;
            case 1 :
                deleteAllUsersIfRequestIsFromAdmin(username);
                break;
            case 2 :
                quitInterface.quit();
            break;
        }
    }

    private void deleteAllUsersIfRequestIsFromAdmin(String username) {
        if(username.equals("admin")) {
            userDao.deleteAllUsers();
            removeAllUsersFromUsersFolder();
            System.out.println(thickLine());
            askLoginOrNewRegisterOrQuit();

        }else{
            System.out.println("REQUEST DENIED!!!\n You are not Admin. Only Admin can remove all Users from Database");
            menu(username);
        }
    }

    private void deleteLoggedUser(String username) {
        if(username.equals("admin")){
            System.out.println("You are Admin and Admin can not be removed from System!!!");
            menu(username);
        }else if (userDao.deleteLoggedUser(username) == 1) {
            removeUserFromUsersFolder(username);
        } else {
            throw new UserCanNotFoundException();
        }
    }

    private void askLoginOrNewRegisterOrQuit() {
        String loginQuestion = "Want to login with another account? " ;
        String registerQuestion = "Want to register for a new account? ";
        String[] actions = {loginQuestion,registerQuestion,QUIT};
        int chosenIndex = askForChoice("What do you prefer?", actions);

        if(chosenIndex == 0){
            loginUser();
        }else if(chosenIndex == 1){
            registerUser();
        }else{
            quitInterface.quit();
        }
    }

    @Override
    public void registerUser() {
        User registeredUser = new User();
        System.out.println(thickLine());
        System.out.println(thinLine());
        System.out.println(center("Welcome REGISTER Page"));
        System.out.println(thinLine());
        System.out.println(thickLine());
        String username ;

        do{
             username = ask("Enter your username ");
        }while(userDao.isExistUsername(username));

        registeredUser.setUsername(username);
        registeredUser.setFirstName(ask("Enter your first name: "));
        registeredUser.setLastName(ask("Enter your surname: "));
        registeredUser.setPassword(ask("Enter your password: "));
        boolean successRegister = userDao.addUser(registeredUser);

        if (successRegister){
            System.out.println(center("Your registration is successful"));
            System.out.println(thickLine());
            System.out.printf("Dear %s %s .You can login with your unique username = %s and password. Please keep yor info!\n" ,
                              registeredUser.getFirstName(), registeredUser.getLastName(), registeredUser.getUsername());
            System.out.println(center("You will be redirected to the Login Page"));
            writeUserToUsersFolder(registeredUser);
            loginUser();
        }else{
            System.out.println(center("DB Error! Please try later"));
            quitInterface.quit();
        }



    }

    private void writeUserToUsersFolder(User registeredUser) {

        String fileNameByUserName = registeredUser.getUsername();
        try(
                FileOutputStream fileOutput = new FileOutputStream(ABSOLUTE_PATH_TO_USERS.resolve(fileNameByUserName +".txt").toFile());
                ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
        ){
            objectOutput.writeObject(registeredUser);
        } catch(IOException ioException){
            ioException.printStackTrace();
        }
    }

    private void removeUserFromUsersFolder(String deletedUserName) {
        String deletedUserNameTxt= deletedUserName + ".txt";
        Path deletedPath = Path.of(deletedUserNameTxt);

        Path fullPathOfDeletedUserPath = ABSOLUTE_PATH_TO_USERS.resolve(deletedPath);

        try {
            Files.deleteIfExists(fullPathOfDeletedUserPath);
        }catch (IOException e){
           // e.printStackTrace();
            System.out.println("DB ERROR!...");
            quitInterface.quit();
        }

    }

    private void removeAllUsersFromUsersFolder() {
        try {
            Files.walk(ABSOLUTE_PATH_TO_USERS)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }catch (IOException e){
            e.printStackTrace();
        }finally{
            folderService.createUsersMap();
        }

    }


    public void addUserFromFileToDaoUserList(User user){
        userDao.addUser(user);
    }


}
