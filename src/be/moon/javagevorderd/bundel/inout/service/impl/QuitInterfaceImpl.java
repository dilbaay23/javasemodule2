package be.moon.javagevorderd.bundel.inout.service.impl;

import be.moon.javagevorderd.bundel.inout.service.FolderService;
import be.moon.javagevorderd.bundel.inout.service.QuitInterface;

import static be.moon.javagevorderd.bundel.inout.utility.MenuUtility.center;
import static be.moon.javagevorderd.bundel.inout.utility.MenuUtility.tildaLine;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public class QuitInterfaceImpl implements QuitInterface {

    private final FolderService folderService = new FolderServiceImpl();

    @Override
    public void quit() {
        System.out.println(tildaLine());
        System.out.println(tildaLine());
        System.out.println(center("*** GOODBYE :) ***"));
        System.out.println(center("*** SEE YOU LATER ***"));
        System.out.println(tildaLine());
        folderService.createFFBackupMap();
        System.out.println(tildaLine());
        System.out.println(tildaLine());
        System.exit(0);
    }

}
