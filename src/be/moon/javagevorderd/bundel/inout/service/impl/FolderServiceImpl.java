package be.moon.javagevorderd.bundel.inout.service.impl;

import be.moon.javagevorderd.bundel.inout.entitty.User;
import be.moon.javagevorderd.bundel.inout.service.FolderService;

import java.io.*;
import java.nio.file.*;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Objects;

import static be.moon.javagevorderd.bundel.inout.config.AppConstants.*;

/**
 * Created By Moon
 * 2/13/2021, Sat
 **/
public class FolderServiceImpl implements FolderService {

    public void createFlopMap() {
        File flopMap = new File(PATH_TO_DESKTOP + "\\FLOP");
        if (!flopMap.exists()){
            flopMap.mkdirs();
        }
    }

    public void createUsersMap() {
        File usersMap = new File(ABSOLUTE_PATH_TO_FLOP + "\\Users");
        if (!usersMap.exists()){
            usersMap.mkdirs();
        }
    }

    public void createFFBackupMap() {
        String pathOfCopyFolder = "\\FF-BACKUP";
        File FFCopyMap = new File(PATH_TO_DESKTOP + pathOfCopyFolder);
        if (!FFCopyMap.exists()){
            FFCopyMap.mkdirs();

        }else{
            cleanFoldersInFFBackupMap();
        }
        createFFBackupWithDate();
    }

    private void cleanFoldersInFFBackupMap() {
        try {
            Files.walk(ABSOLUTE_PATH_TO_FFBACKUP)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void createFFBackupWithDate() {
       LocalDate date = LocalDate.now();
        int dayOfMonth = date.getDayOfMonth();
        int month = date.getMonthValue();
        String pathOfCopyFolder = "\\FF-BACKUP-" +dayOfMonth +"-"+ month ;
        File FFBackupDdMm = new File(ABSOLUTE_PATH_TO_FFBACKUP + pathOfCopyFolder);
        if (!FFBackupDdMm.exists()){
            FFBackupDdMm.mkdirs();
        }
        copyFlopToFFBackupWithDateFolder(FFBackupDdMm);
    }

    private void copyFlopToFFBackupWithDateFolder(File pathOfCopyFolder)  {
        try {
            Files.walk(Paths.get(String.valueOf(ABSOLUTE_PATH_TO_FLOP)))
                    .forEach(source -> {
                        Path destination = Paths.get(String.valueOf(pathOfCopyFolder), source.toString()
                                .substring(ABSOLUTE_PATH_TO_FLOP.toString().length()));
                        try {
                            Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);

                        } catch (IOException e) {
                            e.printStackTrace();

                        }
                    });
            System.out.println("INFO: Your information in the FLOP file has been successfully backed up to the FFBACKUP file");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }





    public HashMap<String, User> listFilesForFolder(File folder){
        HashMap<String, User> userHashMap = new HashMap<>();

        if(folder.isDirectory()) {


            for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
                // System.out.println(fileEntry.toString());
                if (fileEntry.isDirectory()) {
                    //  System.out.println(fileEntry.toString());
                    listFilesForFolder(fileEntry);
                } else {
                    String absoluteFileName = (fileEntry.getPath()).toString();
                    User user = readUserObject(absoluteFileName);
                    userHashMap.put(absoluteFileName, user);
                }
            }
        }
        return userHashMap;
    }

    public User readUserObject(String fileName){
        try(
                FileInputStream fileInput = new FileInputStream(Path.of(fileName).toFile());
                ObjectInputStream objectInput = new ObjectInputStream(fileInput);
        ){
            User inputPerson = (User) objectInput.readObject();
            return inputPerson;
        } catch (IOException | ClassNotFoundException exception){
            exception.printStackTrace();
        }
        return null;

    }
}
