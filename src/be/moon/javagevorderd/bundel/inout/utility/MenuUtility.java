package be.moon.javagevorderd.bundel.inout.utility;

public abstract class MenuUtility {
    private final static int FULL_WIDTH = 42;

    public static void printTitle(String titel) {
        String lijn = generateLine('=', titel.length());
        String titleNice = String.format("%s%n%s%n%s", lijn, titel, lijn);
        System.out.println(titleNice);
    }

    public static void printSubheading(String titel) {
        String lijn = generateLine('-', titel.length());
        String titleNice = String.format("%s%n%s%n%s", lijn, titel, lijn);
        System.out.println(titleNice);
    }

    public static String thickLine(){
        return generateCharNTimes('*', FULL_WIDTH);
    }

    public static String tildaLine(){
        return generateCharNTimes('~', FULL_WIDTH);
    }

    public static String thinLine(){
        return generateCharNTimes('-', FULL_WIDTH);
    }

    private static String generateCharNTimes(char c, int amount){
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < amount; i++) {
            text.append(c);
        }
        return text.toString();
    }

    public static String center(String text){
        return String.format("%" + (FULL_WIDTH/2 + text.length()/2) + "s", text);
    }
    public static String generateLine(char c, int length) {
        StringBuilder lijnAsSB = new StringBuilder();
        for (int i = 0; i < length; i++) {
            lijnAsSB.append(c);
        }
        return lijnAsSB.toString();
    }



}
