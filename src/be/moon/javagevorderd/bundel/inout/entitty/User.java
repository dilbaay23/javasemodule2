package be.moon.javagevorderd.bundel.inout.entitty;

import java.io.Serializable;

/**
 * Created By Moon
 * 2/12/2021, Fri
 **/
public class User implements Serializable {
    private static long NUMBER_OF_USER = 0;
    private String username;
    private String firstName;
    private String lastName;
    private  String password;
  //  private transient String password;

    public User() {

    }

    public User(String username,String firstName, String lastName, String password) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "username =" + username +'\'' +
                ", password =" + password +'\'' +
                ", firstName ='" + firstName + '\'' +
                ", lastName ='" + lastName + '\'' +
                '}';
    }
}
