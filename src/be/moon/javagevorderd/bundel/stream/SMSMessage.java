package be.moon.javagevorderd.bundel.stream;


import java.time.LocalDateTime;

/**
 * Created By Moon
 * 2/7/2021, Sun
 **/
public class SMSMessage {

    private String answer;
    private LocalDateTime smsSendDateTime;
    private PhoneNumber phoneNumber;

    public SMSMessage(String answer, LocalDateTime smsSendDateTime, PhoneNumber phoneNumber) {
        this.answer = answer;
        this.smsSendDateTime = smsSendDateTime;
        this.phoneNumber = phoneNumber;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public LocalDateTime getSmsSendDateTime() {
        return smsSendDateTime;
    }

    public void setSmsSendDateTime(LocalDateTime smsSendDateTime) {
        this.smsSendDateTime = smsSendDateTime;
    }




}
