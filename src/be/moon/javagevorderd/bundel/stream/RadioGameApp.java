package be.moon.javagevorderd.bundel.stream;

import be.moon.javagevorderd.bundel.stream.repo.SmsInfoDAO;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

/**
 * Created By Moon
 * 2/7/2021, Sun
 **/
public class RadioGameApp {
    public static void main(String[] args) {

        final String QUESTION = "What island is Jakarta located in?";
        final String RIGHT_ANSWER = "Indonesia";
        final int NUMBER_OF_WINNERS = 3;
        final LocalDateTime START_TIME = LocalDateTime.of(2021, 2, 5, 17, 45, 01);
        final LocalDateTime END_TIME = LocalDateTime.of(2021, 2, 5, 18, 30, 00);

        askTheQuestion(QUESTION,START_TIME,END_TIME);
        hourglass(7,'*');

        SmsInfoDAO smsInfoDAO = new SmsInfoDAO();

        SMSMessage[] SMSMessageArray = smsInfoDAO.getSmsArray();

        Stream<SMSMessage> stream = Stream.of(SMSMessageArray);
        stream
                .filter(SMSMessage -> SMSMessage.getAnswer().equalsIgnoreCase(RIGHT_ANSWER))
                .filter(SMSMessage -> SMSMessage.getSmsSendDateTime().isAfter(START_TIME) && SMSMessage.getSmsSendDateTime().isBefore(END_TIME))
                //.sorted(Comparator.comparing(SMSMessage::getSmsSendDateTime))
                .map(SMSMessage::getPhoneNumber)
                .distinct()
                .limit(NUMBER_OF_WINNERS)
                .forEach(PhoneNumber::sendCongratulatoryMessage);

    }

    private static void askTheQuestion(String question, LocalDateTime start_time, LocalDateTime end_time) {
        System.out.println("HELLO,HELLO,HELLO");
        System.out.print("Make Me A Winner running from the Start Date --->"+ formatLocalDateTime(start_time));
        System.out.println(" to the End Date --->" + formatLocalDateTime(end_time));
        System.out.println("Question is: " + question);
    }

    private static String formatLocalDateTime(LocalDateTime time) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        return time.format(formatter);
    }
    public static void hourglass(int size, char symbol){

        for(int i=size-1;i>=1;i--){

            for(int j=i;j<size;j++)

                System.out.print(" ");

            for(int j=1;j<=i*2-1;j++)

                System.out.print(symbol);

            System.out.println();
        }

        for(int i=2;i<=size-1;i++)
        {

            for(int j=i;j<size;j++)
                System.out.print(" ");

            for(int j=1;j<=i*2-1;j++)

                System.out.print(symbol);
            System.out.println();

        }

    }


}
