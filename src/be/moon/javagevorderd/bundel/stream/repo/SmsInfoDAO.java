package be.moon.javagevorderd.bundel.stream.repo;

import be.moon.javagevorderd.bundel.stream.PhoneNumber;
import be.moon.javagevorderd.bundel.stream.SMSMessage;

import java.time.LocalDateTime;

/**
 * Created By Moon
 * 2/7/2021, Sun
 **/
public class SmsInfoDAO {
    PhoneNumber phoneNumber1 = new PhoneNumber("Moon","As","04885879636");
    PhoneNumber phoneNumber2 = new PhoneNumber("Star","Der","04885447776");
    PhoneNumber phoneNumber3 = new PhoneNumber("Mars","Asz","04555589945");
    PhoneNumber phoneNumber4 = new PhoneNumber("World","Ars","04821479636");
    PhoneNumber phoneNumber5 = new PhoneNumber("Sun","Ahs","0478545879636");
    PhoneNumber phoneNumber6 = new PhoneNumber("Jupiter","Aks","04812369636");
    PhoneNumber phoneNumber7 = new PhoneNumber("Merkur","Ays","04885872154");
    PhoneNumber phoneNumber8 = new PhoneNumber("Saturn","Avs","048858747856");
    PhoneNumber phoneNumber9 = new PhoneNumber("Venus","Abs","0488583698");
    PhoneNumber phoneNumber10 = new PhoneNumber("Sara","Asya","04758279636");
    PhoneNumber phoneNumber11 = new PhoneNumber("Lira","Asr","04985763222");
    PhoneNumber phoneNumber12 = new PhoneNumber("Ceza","Asu","048214475786");
    PhoneNumber phoneNumber13 = new PhoneNumber("Duman","Asm","04812796256");
    PhoneNumber phoneNumber14 = new PhoneNumber("Kargo","Asa","04845556536");
    PhoneNumber phoneNumber15 = new PhoneNumber("Sila","Ase","0487895544456");
    PhoneNumber phoneNumber16 = new PhoneNumber("Bart","Asur","044252574456");

    SMSMessage SMSMessage1 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 11, 45), phoneNumber1);
    SMSMessage SMSMessage2 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 12, 55), phoneNumber2);
    SMSMessage SMSMessage3 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 13, 42), phoneNumber3);
    SMSMessage SMSMessage4 = new SMSMessage("china", LocalDateTime.of(2021, 2, 5, 18, 14, 35), phoneNumber4);
    SMSMessage SMSMessage5 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 12, 22), phoneNumber5);
    SMSMessage SMSMessage6 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 11, 59), phoneNumber6);
    SMSMessage SMSMessage7 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 11, 55), phoneNumber7);
    SMSMessage SMSMessage8 = new SMSMessage("Africa", LocalDateTime.of(2021, 2, 5, 18, 11, 22), phoneNumber8);
    SMSMessage SMSMessage9 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 17, 47, 23), phoneNumber9);
    SMSMessage SMSMessage10 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 06, 33), phoneNumber10);
    SMSMessage SMSMessage11 = new SMSMessage("America", LocalDateTime.of(2021, 2, 5, 18, 10, 12), phoneNumber11);
    SMSMessage SMSMessage12 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 17, 55, 3), phoneNumber12);
    SMSMessage SMSMessage13 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 6, 18, 9, 56), phoneNumber13);
    SMSMessage SMSMessage14 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 10, 58), phoneNumber14);
    SMSMessage SMSMessage15 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 25, 36), phoneNumber15);
    SMSMessage SMSMessage16 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 14, 47), phoneNumber16);
    SMSMessage SMSMessage17 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 15, 59), phoneNumber5);
    SMSMessage SMSMessage18 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 18, 10, 18), phoneNumber9);
    SMSMessage SMSMessage19 = new SMSMessage("indonesia", LocalDateTime.of(2021, 2, 5, 20, 11, 2), phoneNumber13);

    SMSMessage[] SMSMessageArray = {SMSMessage1, SMSMessage2, SMSMessage3, SMSMessage4, SMSMessage5, SMSMessage6, SMSMessage7, SMSMessage8, SMSMessage9, SMSMessage10, SMSMessage11, SMSMessage12, SMSMessage13, SMSMessage14, SMSMessage15, SMSMessage16, SMSMessage17, SMSMessage18, SMSMessage19};

    public SMSMessage[] getSmsArray() {
        return SMSMessageArray;
    }
}
