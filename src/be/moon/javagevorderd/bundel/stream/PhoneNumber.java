package be.moon.javagevorderd.bundel.stream;

/**
 * Created By Moon
 * 2/7/2021, Sun
 **/
public class PhoneNumber {
    private String firstName;
    private String lastname;
    private String phoneNumber;

    public PhoneNumber(String firstName, String lastname, String phoneNumber) {
        this.firstName = firstName;
        this.lastname = lastname;
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public void sendCongratulatoryMessage(){
        System.out.printf("Dear %s %s,\n Congratulations! You became one of the three people who gave the correct answer to our question.\n", getFirstName(), getLastname());
        System.out.println("***********************************************************************************************************");

    }
}
