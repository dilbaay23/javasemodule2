package be.moon.hs21;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created By Moon
 * 2/11/2021, Thu
 **/
public class PathDemoApp {
    public static void main(String[] args) throws IOException {
        Path fullPathOfHelloWorld = Path.of("C:\\Users\\dilba\\Desktop\\Temp");   // i should use Masaustu instead of Desktop
       System.out.println(fullPathOfHelloWorld);

        Path fullPathOfHelloWorld2 = Path.of("C:","Users","dilba","Masaüstü","Temp");
        System.out.println(fullPathOfHelloWorld2);

        String userHomeDirectory= System.getProperty("user.home");
        System.out.println(userHomeDirectory);

        String userName= System.getProperty("user.name");
        System.out.println(userName);

        String osVersion= System.getProperty("os.version");
        System.out.println(osVersion);

       // Path pathToDesktop = Path.of(userHomeDirectory,"Desktop");
        Path pathToMasaustu= Path.of(userHomeDirectory,"Masaüstü");
        Path pathToHelloWorld = Path.of("Temp","HelloWorld.txt");
       // fullPathOfHelloWorld2 =pathToDesktop.resolve(pathToHelloWorld);
        fullPathOfHelloWorld2 =pathToMasaustu.resolve(pathToHelloWorld);
       //System.out.println(fullPathOfHelloWorld);

        Files.deleteIfExists(fullPathOfHelloWorld2);

        String message = "Hello World";
        try {
            Files.createFile(fullPathOfHelloWorld2);
            Files.writeString(fullPathOfHelloWorld2,message);

        }catch (IOException e){
            e.printStackTrace();
        }



    }
}
