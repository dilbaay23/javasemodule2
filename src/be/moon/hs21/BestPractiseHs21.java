package be.moon.hs21;

import be.moon.util.TextUtil;
import be.moon.util.person.Person;
import be.moon.util.person.PersonGenerator;

import java.io.*;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import java.util.stream.Collectors;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * Created By Moon
 * 2/11/2021, Thu
 **/
public class BestPractiseHs21 {

    public static final Path PATH_TO_DESKTOP = Path.of(System.getProperty("user.home"), "Masaüstü");
    public static final Path PATH_TO_TEMP = Path.of("Temp");
    public static final Path PATH_TO_HELLO_WORLD = Path.of("Temp", "HelloWorld.txt");
    public static final Path FULL_PATH_TO_HELLO_WORLD = PATH_TO_DESKTOP.resolve(PATH_TO_HELLO_WORLD);
    public static final Path ABSOLUTE_PATH_TO_TEMP = PATH_TO_DESKTOP.resolve(PATH_TO_TEMP);
    public static final Path ABSOLUTE_PATH_TO_HELLO_WORLD_TXT = ABSOLUTE_PATH_TO_TEMP.resolve(PATH_TO_HELLO_WORLD);


    public static void main(String[] args) {

          //  opdracht2TryToWriteToFile();
          //  opdracht3TryToReadFile();
          //  opdracht4TryToBufferedReadAndWriteFile();
         //   opdracht5TryToCompressByteStream();
        opdracht6TryToObjectByteStream();


        }

    private static void opdracht6TryToObjectByteStream() {
        TextUtil.printTitle("Opdracht 6");
        TextUtil.printSubheading("Object stream");


        Person person = PersonGenerator.generate();


        System.out.printf("Writing texts %s, converting to bytes, compressing (deflating) to %s...%n", "person", ABSOLUTE_PATH_TO_HELLO_WORLD_TXT);
        try (
                FileOutputStream fileOutput= new FileOutputStream(ABSOLUTE_PATH_TO_TEMP.resolve("person.txt").toFile());
              //  FileOutputStream fileOutput= new FileOutputStream(ABSOLUTE_PATH_TO_TEMP.resolve("person.mickeymouse").toFile());     ; .mickeymouse can be too:)
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutput);
        ) {
            objectOutputStream.writeObject(person);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }


        System.out.printf("Reading file %s, de-compressing (inflating), converting from bytes to text, line by line, via ObjectInputStream...%n", ABSOLUTE_PATH_TO_HELLO_WORLD_TXT);
        try (
                FileInputStream reader = new FileInputStream(ABSOLUTE_PATH_TO_TEMP.resolve("person.txt").toFile());
           ObjectInputStream objectInputStream = new ObjectInputStream(reader);
        ) {
            Person inputPerson = (Person) objectInputStream.readObject();
            System.out.println(inputPerson);
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
        }
    }

        private static void opdracht5TryToCompressByteStream() {
            TextUtil.printTitle("Opdracht 5");
            TextUtil.printSubheading("Byte stream");

            List<String> textsToCompress = PersonGenerator.generatePeople(50)
                    .stream()
                    .map(p -> p.getFirstName() + " " + p.getSurname())
                    .collect(Collectors.toList());

            System.out.printf("Writing texts %s, converting to bytes, compressing (deflating) to %s...%n", "[People]", ABSOLUTE_PATH_TO_HELLO_WORLD_TXT);
            try (
                    FileOutputStream fileOutputStream = new FileOutputStream(ABSOLUTE_PATH_TO_HELLO_WORLD_TXT.toFile());
                    DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(fileOutputStream);
                    PrintStream printStream = new PrintStream(deflaterOutputStream);
            ) {
                for (String textToCompress : textsToCompress) {
                    printStream.write(textToCompress.getBytes());
                    printStream.write("\n".getBytes());
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            System.out.printf("Reading file %s, de-compressing (inflating), converting from bytes to text, line by line, via BufferedReader...%n", ABSOLUTE_PATH_TO_HELLO_WORLD_TXT);
            try (
                    FileInputStream reader = new FileInputStream(ABSOLUTE_PATH_TO_HELLO_WORLD_TXT.toFile());
                    InflaterInputStream inflater = new InflaterInputStream(reader);
                    InputStreamReader inputToReader = new InputStreamReader(inflater); // Converting ByteStream to Character Stream
                    BufferedReader bufferedReader = new BufferedReader(inputToReader);
            ) {
                bufferedReader.lines()
                        .forEach(System.out::println);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }


        private static void opdracht4TryToBufferedReadAndWriteFile() {
        TextUtil.printTitle("Opdracht 4");
        TextUtil.printSubheading("Buffered reader");
        System.out.printf("Reading %s line by line using BufferedReader...%n", FULL_PATH_TO_HELLO_WORLD);
        try(
                FileReader fileReader = new FileReader(FULL_PATH_TO_HELLO_WORLD.toFile());
                BufferedReader bufferedReader = new BufferedReader(fileReader);
        ){
            String line;
            while((line = bufferedReader.readLine()) != null){
                System.out.println(line);
            }
        } catch (IOException ioException){
            ioException.printStackTrace();
        }
        TextUtil.printSubheading("Buffered writer");
        String[] planets = {"Earth", "Mars", "Jupiter", "Venus"};
        System.out.printf("Writing %s to %s line by line using Buffered File Writer...%n", Arrays.asList(planets), FULL_PATH_TO_HELLO_WORLD);
        try(
                FileWriter fileWriter = new FileWriter(FULL_PATH_TO_HELLO_WORLD.toFile());
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        ){
            for (String planet : planets) {
                bufferedWriter.write("Hello " + planet);
                bufferedWriter.newLine();
            }
        } catch (IOException ioException){
            ioException.printStackTrace();
        }

    }

    private static void opdracht3TryToReadFile() {
        TextUtil.printTitle("Opdracht 3");
        TextUtil.printSubheading("File reader");
        System.out.printf("Reading %s character by character using File Reader...%n", FULL_PATH_TO_HELLO_WORLD);
        try(
                FileReader fileReader = new FileReader(FULL_PATH_TO_HELLO_WORLD.toFile());
        ){
            int character;
            while((character = fileReader.read()) != -1){
                System.out.println((char) character);
            }
        } catch (IOException ioException){
            ioException.printStackTrace();
        }
    }

    private static void opdracht2TryToWriteToFile() {
        TextUtil.printTitle("Opdracht 2");
        TextUtil.printSubheading("File writer");
        System.out.printf("Writing to %s character by character using File Writer...%n", FULL_PATH_TO_HELLO_WORLD);
        try (
                FileWriter fileWriter = new FileWriter(FULL_PATH_TO_HELLO_WORLD.toFile());
        ){
            fileWriter.write("Hello");
            fileWriter.write("World");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
