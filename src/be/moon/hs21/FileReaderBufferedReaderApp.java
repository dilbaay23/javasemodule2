package be.moon.hs21;

import be.moon.util.TextUtil;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Created By Moon
 * 2/11/2021, Thu
 **/
public class FileReaderBufferedReaderApp {
    public static void main(String[] args) {
        String pathToHomePath = System.getProperty("user.home");
        String fullPathToHelloWorldString = pathToHomePath + "\\Masaüstü\\Temp\\v1.txt";

        Path pathToMasaustu= Path.of(pathToHomePath,"Masaüstü");
        Path pathToHelloWorld2 = Path.of("Temp","HelloWorld.txt");
        Path fullPathOfHelloWorld2 =pathToMasaustu.resolve(pathToHelloWorld2);

        Path pathToV1 = Path.of("Temp","v1.txt");
        Path fullPathOfV1 =pathToMasaustu.resolve(pathToV1);

        TextUtil.printSubheading("File reader");
        System.out.printf("Reading %s character by character using File Reader...%n", fullPathOfHelloWorld2);
        try(FileReader fileReader = new FileReader(fullPathOfHelloWorld2.toFile())) {
            int character ;
            while((character = fileReader.read()) != -1){
                System.out.println((char) character);
        }
        } catch (IOException e) {
            e.printStackTrace();
        }

        TextUtil.printTitle("Opdracht 4");
        TextUtil.printSubheading("Buffered reader");
        System.out.printf("Reading %s line by line using BufferedReader...%n", fullPathOfHelloWorld2);
        try(
                FileReader fileReader = new FileReader(fullPathOfHelloWorld2.toFile());
                BufferedReader bufferedReader = new BufferedReader(fileReader);
        ){
            String line;
            while((line = bufferedReader.readLine()) != null){
                System.out.println(line);
            }
        } catch (IOException ioException){
            ioException.printStackTrace();
        }




    }
}
