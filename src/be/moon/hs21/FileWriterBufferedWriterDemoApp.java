package be.moon.hs21;

import be.moon.util.TextUtil;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

/**
 * Created By Moon
 * 2/11/2021, Thu
 **/
public class FileWriterBufferedWriterDemoApp {
    public static void main(String[] args)  {

        String pathToHomePath = System.getProperty("user.home");
        String fullPathToHelloWorldString = pathToHomePath + "\\Masaüstü\\Temp\\v1.txt";

        Path pathToMasaustu= Path.of(pathToHomePath,"Masaüstü");
        Path pathToHelloWorld2 = Path.of("Temp","HelloWorld.txt");
        Path fullPathOfHelloWorld2 =pathToMasaustu.resolve(pathToHelloWorld2);

        Path pathToV1 = Path.of("Temp","v1.txt");
        Path fullPathOfV1 =pathToMasaustu.resolve(pathToV1);


        //op 2 puntje 4-5-6


        try(FileWriter fileWriter1 = new FileWriter(fullPathOfHelloWorld2.toFile(),true);
            FileWriter fileWriter2 = new FileWriter(fullPathOfV1.toFile())){

            fileWriter1.write(" Append the  text. ");
            fileWriter2.write("Override the text ");
        }catch(IOException e){
            e.printStackTrace();
        }

        TextUtil.printSubheading("Buffered writer");
        String[] planets = {"Earth", "Mars", "Jupiter", "Venus"};
        System.out.printf("Writing %s to %s line by line using Buffered File Writer...%n", Arrays.asList(planets), pathToHelloWorld2);
        try(
                FileWriter fileWriter = new FileWriter(pathToHelloWorld2.toFile());
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        ){
            for (String planet : planets) {
                bufferedWriter.write("Hello " + planet);
                bufferedWriter.newLine();
            }
        } catch (IOException ioException){
            ioException.printStackTrace();
        }


        /*
        //op 2 puntje 1-2-3

        FileWriter fileWriter = null;
        try{
           // fileWriter = new FileWriter(fullPathToHelloWorld);
            fileWriter = new FileWriter(fullPathOfHelloWorld2.toFile(),true);    // Path object .toFile()
            fileWriter.write("New ");
            fileWriter.write("World. ");
        }catch(IOException e){
            e.printStackTrace();
        }finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

         */

    }
}
