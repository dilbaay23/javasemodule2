package be.moon.hs14;

/**
 * Created By Moon
 * 2/2/2021, Tue
 **/
public class MusicianApp {
    public static void main(String[] args) {

        Musician mozart = new Musician("Mozart");
        mozart.play(new Instrument() {
            @Override
            public void makeSound() {
                System.out.println("in method impl...");
            }
        });

       /* Musician.Instrument piano = mozart.new Instrument();
        piano.makeSound();*/
        mozart.play(() -> System.out.println("Trelloooo"));

    }

}
