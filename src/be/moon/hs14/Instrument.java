package be.moon.hs14;

/**
 * Created By Moon
 * 2/2/2021, Tue
 **/
public interface Instrument {
    void makeSound();
}
