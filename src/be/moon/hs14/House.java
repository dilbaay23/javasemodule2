package be.moon.hs14;

/**
 * Created By Moon
 * 2/2/2021, Tue
 **/
public class House {
    private Room kitchen;
    private Room bedroom;
    private Room livingRoom;

    private class Room{
        private double metersquare;

        public double getMetersquare() {
            return metersquare;
        }

        public void setMetersquare(double metersquare) {
            this.metersquare = metersquare;
        }

        @Override
        public String toString() {
            return "Room{" +
                    "metersquare=" + metersquare +
                    '}';
        }
    }

    public Room getRoom(double sm){
        if(sm<60) {
            Room room = new Room();
            room.setMetersquare(sm);
            return room;
        }else
            System.out.println("It is a very big room...");
        return null;
    }


    public Room getKitchen() {
        return kitchen;
    }

    public void setKitchen(Room kitchen) {
        this.kitchen = kitchen;
    }

    public Room getBedroom() {
        return bedroom;
    }

    public void setBedroom(Room bedroom) {
        this.bedroom = bedroom;
    }

    public Room getLivingRoom() {
        return livingRoom;
    }

    public void setLivingRoom(Room livingRoom) {
        this.livingRoom = livingRoom;
    }

    @Override
    public String toString() {
        return "House{" +
                "kitchen=" + kitchen + kitchen.getMetersquare() +
                ", bedroom=" + bedroom + bedroom.getMetersquare() +
                ", livingRoom=" + livingRoom + livingRoom.getMetersquare()+
                '}' ;
    }
}
