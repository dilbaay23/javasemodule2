package be.moon.hs14;

/**
 * Created By Moon
 * 2/2/2021, Tue
 **/
public class NestedClassesDemo {
    public static void main(String[] args) {
        House myHouse= new House();

        myHouse.setBedroom(myHouse.getRoom(20));
        myHouse.setKitchen(myHouse.getRoom(30));
        myHouse.setLivingRoom(myHouse.getRoom(70));
        System.out.println(myHouse.getBedroom());
        System.out.println(myHouse.getKitchen());
        System.out.println(myHouse.getLivingRoom());
    }
}
