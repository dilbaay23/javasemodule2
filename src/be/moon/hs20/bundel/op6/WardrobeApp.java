package be.moon.hs20.bundel.op6;

import java.util.*;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class WardrobeApp {
    public static void main(String[] args) {

        List<Coat> wardrobe = new ArrayList<>();
        wardrobe.add(new Coat("Gucci", "red"));
        wardrobe.add(new Coat("Louis Vitton", "blue"));
        wardrobe.add(new Coat("Chanel", "grey"));
        wardrobe.add(new Coat("Lacoste", "green"));
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Give a description of your coat: ");
        String inputDescription = keyboard.nextLine();
        for (Coat coat : wardrobe) {
            System.out.println("Is this your coat?");
            if (inputDescription.equals(coat.toString())){
                System.out.printf("Yes! This is my %s%n", coat);
                break;
            } else {
                System.out.printf("No...this is not my %s%n", coat);
            }
        }
        System.out.println("*********************");

        Map<Long, Coat> wardrobeImproved = new HashMap<>();
        wardrobeImproved.put(1L, new Coat("Gucci", "red"));
        wardrobeImproved.put(2L, new Coat("Louis Vitton", "blue"));
        wardrobeImproved.put(3L, new Coat("Chanel", "grey"));
        wardrobeImproved.put(4L, new Coat("Lacoste", "green"));
        wardrobeImproved.put(5L, new Coat("Mexx", "white"));
      //  wardrobeImproved.put(5L, new Coat("Gucci", "white"));
        wardrobeImproved.forEach((k,v) -> System.out.println(v));

        System.out.println("Give me your ticket id: ");
        Long idTicket = Long.valueOf(keyboard.nextLine());
        System.out.printf("Found! Here you go, sir/madam, your %s", wardrobeImproved.get(idTicket));

    }

}
