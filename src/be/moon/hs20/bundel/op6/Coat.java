package be.moon.hs20.bundel.op6;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class Coat {
        private String brand;
        private String colour;

        public Coat(String brand, String colour) {
            this.brand = brand;
            this.colour = colour;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getColour() {
            return colour;
        }

        public void setColour(String colour) {
            this.colour = colour;
        }

        @Override
        public String toString() {
            return getColour() + " " + getBrand();
        }


}
