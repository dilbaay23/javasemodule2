package be.moon.hs20.bundel.op2;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class ArrayForm {
    public static void main(String[] args) {
        String[] watchedFilms = {"Notting Hill","Avengers", "Erin Brockovich","Preety Women"};
       /* watchedFilms= add(watchedFilms,"Eat,Pray,Love");
        Arrays.stream(watchedFilms)
                .forEach(System.out::println);*/

        Scanner scanner = new Scanner(System.in);
        boolean cont = true;

        do {
            System.out.println("Give a film that you have seen");
            String newFilm = scanner.next();
            watchedFilms= add(watchedFilms,newFilm);
            Arrays.stream(watchedFilms)
                    .forEach(System.out::println);
            System.out.println("Do you want to give an other film(yes/no)");
            String answer = scanner.next();
            if(answer.equalsIgnoreCase("no"))
                cont=false;

        } while (cont);


    }
    static String[] add(String[] watchedFilms, String element) {
     String[] newFilmArray = new String[watchedFilms.length+1];
        for (int i = 0; i < watchedFilms.length; i++) {
            newFilmArray[i]=watchedFilms[i];
        }
        newFilmArray[watchedFilms.length]= element;
        return newFilmArray;

    }
}
