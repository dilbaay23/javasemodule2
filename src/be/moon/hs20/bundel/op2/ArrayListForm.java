package be.moon.hs20.bundel.op2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class ArrayListForm {
    public static void main(String[] args) {

        String[] watchedFilms = {"Notting Hill","Avengers", "Erin Brockovich","Preety Women"};

        Scanner scanner = new Scanner(System.in);

        ArrayList<String> wathedFilmArrayList = new ArrayList<>();
        System.out.println(wathedFilmArrayList);
        wathedFilmArrayList.addAll(Arrays.asList(watchedFilms));
        boolean go = true;

        do {
            System.out.println("Give a film that you have seen");
            String newFilm = scanner.next();
            wathedFilmArrayList.add(newFilm);
            System.out.println(wathedFilmArrayList);
            System.out.println("Do you want to give an other film(yes/no)");
            String answer = scanner.next();
            if(answer.equalsIgnoreCase("no"))
                go=false;

        } while (go);
    }
}
