package be.moon.hs20.bundel.op1;

import java.util.ArrayList;


/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class Opdracht1Demo {
    public static void main(String[] args) {
        // int[] intArray = {2,3,5,7};
        // intArray[5]=7;

        ArrayList<Integer> intList = new ArrayList<>();
        intList.add(2);
        intList.add(3);
        intList.add(5);
        intList.add(7);
        System.out.println(intList);
        changeList(intList, 0,11);
        System.out.println(intList);
        changeList(intList, 0,2);
        System.out.println(intList);
        intList.add(0,1);
        System.out.println(intList);


    }

    static void changeList(ArrayList<Integer> arrayList, int indexForChange,int newValue) {
        arrayList.remove(indexForChange); //remove it
        arrayList.add(indexForChange, newValue); //place the value back in the arrayList
    }
}
