package be.moon.hs20.bundel.op4;

import be.moon.hs19.opdrachten.Gender;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;

/**
 * Created By Moon
 * 2/5/2021, Fri
 **/
public class Person  implements Comparable<Person>{
    private String name;
    private String surname;
    private Gender gender;
    private LocalDate dob;



    public Person(String name, String surname, Gender gender, LocalDate dob) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.dob = dob;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }



    public long calculateAge(){
        return ChronoUnit.YEARS.between(dob,LocalDate.now());
    }



    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", gender=" + gender +
                ", age=" + calculateAge() +
                '}';
    }

  /*  @Override
    public int compareTo(Person otherPerson) {
        return (int) ( otherPerson.calculateAge()-this.calculateAge() );
    }*/
    @Override
    public int compareTo(Person o){
        return Comparator.comparing(Person::calculateAge)
                .thenComparing(Person::getSurname)
                .thenComparing(Person::getName)
                .compare(o, this);
    }


}
