package be.moon.hs20.bundel.op4;

import java.time.LocalDate;
import java.util.*;

import static be.moon.hs19.opdrachten.Gender.FEMALE;
import static be.moon.hs19.opdrachten.Gender.MALE;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class TreeSetApp {
    public static void main(String[] args) {
        Person[] people = {new Person("Moon", "KOC", FEMALE, LocalDate.of(1980, 11, 5)),
                new Person("Star", "AS", FEMALE, LocalDate.of(1985, 10, 5)),
                new Person("World", "DEF", FEMALE, LocalDate.of(1990, 1, 5)),
                new Person("Mars", "ZED", MALE, LocalDate.of(1970, 3, 5)),
                new Person("Mars2", "ZED", MALE, LocalDate.of(1970, 3, 5)),
                new Person("Jupiter", "ZAS", FEMALE, LocalDate.of(1975, 5, 2))};

        Set<Person> personSet = new TreeSet<>(Arrays.asList(people));

        //we dont have to override compareTo method in Person class. we implement here how it will sort but it will not take the same ages. because it is set. it will think they are same objects...
        Set<Person> personSetCompareImplementation = new TreeSet<>((p1,p2) -> (int) (p2.calculateAge()-p1.calculateAge()));
        personSetCompareImplementation.addAll(Arrays.asList(people));

        personSetCompareImplementation.add(new Person("Bart", "New", FEMALE, LocalDate.of(1962, 1, 5)));
        personSetCompareImplementation.add(new Person("C2", "New", FEMALE, LocalDate.of(1983, 1, 5)));
        personSetCompareImplementation.add(new Person("D2", "New", FEMALE, LocalDate.of(1999, 1, 5)));

        System.out.println(personSet);
        System.out.println(personSetCompareImplementation);


    }
}
