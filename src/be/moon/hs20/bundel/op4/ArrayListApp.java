package be.moon.hs20.bundel.op4;



import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static be.moon.hs19.opdrachten.Gender.FEMALE;
import static be.moon.hs19.opdrachten.Gender.MALE;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class ArrayListApp {
    public static void main(String[] args) {
        Person[] people = {new Person("Moon", "KOC", FEMALE, LocalDate.of(1980, 11, 5)),
                new Person("Star", "AS", FEMALE, LocalDate.of(1985, 10, 5)),
                new Person("World", "DEF", FEMALE, LocalDate.of(1990, 1, 5)),
                new Person("Mars", "ZED", MALE, LocalDate.of(1970, 3, 5)),
                new Person("Jupiter", "ZAS", FEMALE, LocalDate.of(1975, 5, 2))};

        List<Person> personList = new ArrayList<>(Arrays.asList(people));
        personList = sortList(personList);
        printList(personList);

        System.out.println("**************************************************");
        personList.add(new Person("Bart", "New", FEMALE, LocalDate.of(1962, 1, 5)));
        personList.add(new Person("C2", "New", FEMALE, LocalDate.of(1983, 1, 5)));
        personList.add(new Person("D2", "New", FEMALE, LocalDate.of(1999, 1, 5)));

        personList = sortList(personList);
        printList(personList);


    }
    public static List<Person> sortList(List<Person> personList){
        return personList.stream()
               // .sorted(Comparator.comparing(Person::calculateAge))    // this works good too
                .sorted((p1,p2) -> (int) (p1.calculateAge()-p2.calculateAge()))
                .collect(Collectors.toList());

    }

    public static void printList(List<Person> personList){
        for (int i = personList.size() - 1; i >= 0; i--) {
            System.out.println("It is your turn . Your age is : " + personList.get(i));
        }

    }





}
