package be.moon.hs20.bundel.op5;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class WorldWonderApp {
    public static void main(String[] args) {
        List<String> landsOfWonders = new ArrayList<>();
        landsOfWonders.add("China");
        landsOfWonders.add("Jordan");
        landsOfWonders.add("Italy");
        landsOfWonders.add("Mexico");
        landsOfWonders.add("Peru");
        landsOfWonders.add("India");
        landsOfWonders.add("Brazil");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a land name that has one of The Wonder of The World?...");
        String answerLand = scanner.nextLine();

        boolean correctAnswer = false;
        for (String land : landsOfWonders) {
            if(land.equalsIgnoreCase(answerLand))
            {
                correctAnswer =true;
                System.out.println("Congratulations. You answered the question correctly and won a trip to the country where one of the 7 Wonders of the World is ");
                break;
            }
        }
        if (!correctAnswer) {
            System.out.println("Unfortunately, your answer is wrong. See you in other competitions. ");
        }


    }
}
