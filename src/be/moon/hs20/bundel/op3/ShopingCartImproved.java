package be.moon.hs20.bundel.op3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class ShopingCartImproved {
    public static void main(String[] args) {
        Product[] products = {new Book(), new Laptop(), new Book(),new Laptop()};
        List<Product> productArrayList =new ArrayList<>();
        productArrayList.addAll(Arrays.asList(products));
        productArrayList.add(new Laptop());
        productArrayList.add(new Book());
        productArrayList.add(new Laptop());
        System.out.println(productArrayList);
        Optional<Integer> sum = productArrayList.stream()
                .filter(product -> product instanceof Book)
                .map(product -> product.getCost().intValue())
                .reduce(Integer::sum);
        System.out.println("sum = " + sum.get());

    }
}
