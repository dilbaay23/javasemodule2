package be.moon.hs20.bundel.op3;

import java.math.BigDecimal;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class Laptop extends Product {
    private String brand;
    private String series;


    public Laptop(long id, String naem, double weight, BigDecimal cost, String brand, String series) {
        super(id, naem, weight, cost);
        this.brand = brand;
        this.series = series;
    }

    public Laptop() {
    }
}
