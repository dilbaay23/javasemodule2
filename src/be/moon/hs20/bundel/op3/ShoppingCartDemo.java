package be.moon.hs20.bundel.op3;

import java.util.Arrays;
import java.util.stream.Collectors;


/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class ShoppingCartDemo {
    public static void main(String[] args) {
        Product[] products = {new Book(), new Laptop(), new Book(),new Laptop()};
        products =  add(products,new Laptop());
        Arrays.stream(products)
                .forEach(System.out::println);

        System.out.println("***********************");

       Arrays.stream(products)
                .filter(product -> product instanceof Book)
                .map (sc -> (Book) sc)
                .collect(Collectors.toList())
                .forEach(System.out::println);


    }
    static Product[] add(Product[] products, Product element) {
        Product[] newProducts = new Product[products.length+1];
        for (int i = 0; i < products.length; i++) {
            newProducts[i]=products[i];
        }
        newProducts[products.length]= element;
        return newProducts;

    }
}
