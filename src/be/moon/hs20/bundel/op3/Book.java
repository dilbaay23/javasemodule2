package be.moon.hs20.bundel.op3;

import java.math.BigDecimal;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class Book extends Product{
    private String author;
    private String title;
    private String isbn10;
    private String nrOfPages;

    public Book(long id, String naem, double weight, BigDecimal cost, String author, String title, String isbn10, String nrOfPages) {
        super(id, naem, weight, cost);
        this.author = author;
        this.title = title;
        this.isbn10 = isbn10;
        this.nrOfPages = nrOfPages;
    }

    public Book() {
    }
}
