package be.moon.hs20.bundel.op3;

import java.math.BigDecimal;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class Product {
    private long id;
    private String name;
    private double weight;
    private BigDecimal cost=new BigDecimal(10);

    public Product(long id, String name, double weight, BigDecimal cost) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.cost = cost;
    }

    public Product() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }
}
