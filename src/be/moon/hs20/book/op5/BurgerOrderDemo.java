package be.moon.hs20.book.op5;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class BurgerOrderDemo {
    public static void main(String[] args) {
        Queue<BurgerOrder> priortyQueueOrders = new PriorityQueue<>(Comparator.comparing(BurgerOrder::getId).reversed());

        BurgerOrder burgerOrder1 = new BurgerOrder("LargeMac",2);
        BurgerOrder burgerOrder2 = new BurgerOrder("FishMac",1);
        BurgerOrder burgerOrder3 = new BurgerOrder("VeganMac",5);
        BurgerOrder burgerOrder4 = new BurgerOrder("MixMac",3);
        BurgerOrder burgerOrder5 = new BurgerOrder("FamilyMac",1);
        BurgerOrder burgerOrder6 = new BurgerOrder("KindMac",2);
        priortyQueueOrders.add(burgerOrder2);
        priortyQueueOrders.add(burgerOrder3);
        priortyQueueOrders.add(burgerOrder1);
        priortyQueueOrders.add(burgerOrder6);
        priortyQueueOrders.add(burgerOrder4);
        priortyQueueOrders.add(burgerOrder5);
        while(priortyQueueOrders.size() >0){
            System.out.println(priortyQueueOrders.poll());
        }
        System.out.println("********************************************");

        Queue<BurgerOrder> linkedListedOrders = new LinkedList<>();

        linkedListedOrders.add(burgerOrder2);
        linkedListedOrders.add(burgerOrder3);
        linkedListedOrders.add(burgerOrder1);
        linkedListedOrders.add(burgerOrder4);
        linkedListedOrders.add(burgerOrder5);
        linkedListedOrders.add(burgerOrder6);
       /* while(linkedListedOrders.size() >0){
            System.out.println(linkedListedOrders.poll());
        }*/
        linkedListedOrders.forEach(System.out::println);


    }
}
