package be.moon.hs20.book.op1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class ArrayListDemo2 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        boolean flag= true;
        do{
            System.out.println("Enter a word for your sentence!...You can finish your sentence with a word which has a dot at the end of the word!...");
            String word = scanner.next();
            list.add(word);
            if(word.charAt(word.length()-1)=='.')
                flag=false;
        }while(flag);

        System.out.println(list);

        StringBuilder newReversedSentence= new StringBuilder();
        for (int i = list.size()-1; i >=0 ; i--) {
            StringBuilder newReversedWord = new StringBuilder(list.get(i));
            newReversedWord= newReversedWord.reverse();
            newReversedSentence.append(newReversedWord).append(" ");
        }

        System.out.println("My new reversed sentence : " + newReversedSentence);

        System.out.println("Number of words : " +list.stream().count());
        String[] array = list.toArray(new String[list.size()]);
        for (String word: array) {
            System.out.println(word);
        }
    }
}
