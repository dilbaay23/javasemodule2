package be.moon.hs20.book.op1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Created By Moon
 * 2/8/2021, Mon
 **/
public class ArrayListDemo {
    public static void main(String[] args) {

        //List<Integer> list = new ArrayList<>();

        List<Integer> list = new LinkedList<>();

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i++) {
            System.out.println("Enter a number!...");
            int number = scanner.nextInt();
            list.add(number);
        }
        System.out.println(list);
        int sum= list.stream()
                .reduce(Integer::sum).orElse(0);
        System.out.println("sum = " + sum);

        System.out.println("**************************************");

        double avg= list.stream()
                .mapToInt(val -> val)
                .average().orElse(0.0);

        System.out.println("Average = " + avg);


    }
}
