package be.moon.hs20.book.yannick;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class BurgerOrder implements Comparable<BurgerOrder> {
    private static long totalNrOfOrders = 0;
    private long id;
    private String name;
    private int amount;

    public BurgerOrder(String name, int amount) {
        this.id = ++totalNrOfOrders;
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BurgerOrder{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                '}';
    }

    @Override
    public int compareTo(BurgerOrder otherBurgerOrder) {
        if (otherBurgerOrder.getId() < this.getId()) {
            return 1; // this current burger order goes last
        } else if (otherBurgerOrder.getId() > this.getId()) {
            return -1; // this current burger order goes first
        } else {
            return 0; // they have the same ID, sort them as equals during sorting
        }
    }
}
