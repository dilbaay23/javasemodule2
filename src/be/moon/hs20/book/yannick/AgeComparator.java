package be.moon.hs20.book.yannick;



import be.moon.util.person.Person;

import java.util.Comparator;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class AgeComparator implements Comparator<Person> {
    @Override
    public int compare(Person p1, Person p2) {
        return (int) (p1.calcAge() - p2.calcAge());
    }
}
