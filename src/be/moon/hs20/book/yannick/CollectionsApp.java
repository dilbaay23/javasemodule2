package be.moon.hs20.book.yannick;


import be.moon.util.KeyboardUtility;
import be.moon.util.RandomUtility;
import be.moon.util.TextUtil;
import be.moon.util.person.Gender;
import be.moon.util.person.Person;
import be.moon.util.person.PersonGenerator;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class CollectionsApp {
    public static void main(String[] args) {
//        opdracht1();
//        opdracht2();
//        opdracht3();
//        opdracht4();
//        opdracht5();
//        opdracht6();
//        opdracht7();
        opdracht8();
    }

    private static void opdracht8() {
        TextUtil.printTitle("Opdracht 8");
        TextUtil.printSubheading("Singular Sorting");

        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 10 ; i++) {
            people.add(PersonGenerator.generate());
        }
       people.stream()                                                                  //It sorts in stream but original people doesnt change.
               .sorted(Comparator.comparing(Person::getFirstName))
               .forEach(System.out::println);

        TextUtil.printSubheading("After  people sorted in stream try to write original people. see the original  people is not sorted");
        people.forEach(System.out::println);

        TextUtil.printSubheading("after original people sorted");

        people.sort(Comparator.comparing(Person::getFirstName));  // this sort the people for one times.Original list changesfor ones.
        people.forEach(System.out::println);

    }

    private static void opdracht7() {
        TextUtil.printTitle("Opdracht 7");
        TextUtil.printSubheading("People TreeSet with age comparator to sort by age (asc.)");
        Set<Person> peopleAgeAsc = new TreeSet<>(new AgeComparator());
        peopleAgeAsc.add(new Person("Yannick", "Van Ham", Gender.MALE,
                LocalDate.of(1994, Month.NOVEMBER, 23),
                75, 190));
        peopleAgeAsc.add(new Person("Xavier", "Bhambra", Gender.MALE,
                LocalDate.of(1990, Month.FEBRUARY, 26),
                70, 184));
        peopleAgeAsc.add(new Person("Ludo", "Jones", Gender.MALE,
                LocalDate.of(1952, Month.SEPTEMBER, 11),
                80, 175));
        peopleAgeAsc.add(new Person("Martine", "Warmowsky", Gender.FEMALE,
                LocalDate.of(1960, Month.AUGUST, 16),
                65, 170));
        peopleAgeAsc.add(new Person("Yannick", "Van Ham", Gender.MALE,
                LocalDate.of(1994, Month.NOVEMBER, 23),
                75, 190));
        peopleAgeAsc.stream()
                .forEach(System.out::println);

        TextUtil.printSubheading("People TreeSet with anonymous nested Comparator class to sort by weight (asc.)");
        Set<Person> people = new TreeSet<>(
                new Comparator<Person>() {
                    @Override
                    public int compare(Person p1, Person p2) {
                        return (int) (p1.getMass() - p2.getMass());
                    }
                }
        );
        people.addAll(peopleAgeAsc);
        people.stream()
                .forEach(System.out::println);

        TextUtil.printSubheading("People TreeSet with lambda Comparator to sort by weight (asc.)");
        people = new TreeSet<>((p1, p2) -> (int) (p1.getMass() - p2.getMass()));
        people.addAll(peopleAgeAsc);
        people.stream()
                .forEach(System.out::println);

        TextUtil.printSubheading("People TreeSet with lambda Comparator's static 'comparing' method to sort by weight (asc.)");
        people = new TreeSet<>(Comparator.comparing(Person::getMass));
        people.addAll(peopleAgeAsc);
        people.stream()
                .forEach(System.out::println);

        TextUtil.printSubheading("People TreeSet with lambda Comparator's static 'comparing' method to sort by weight (desc.)");
        people = new TreeSet<>(Comparator.comparing(Person::getMass).reversed());
        people.addAll(peopleAgeAsc);
        people.stream()
                .forEach(System.out::println);

        TextUtil.printSubheading("People TreeSet with lambda Comparator's static 'comparing' method to sort by age, then name, then weight");
        people = new TreeSet<>(
                Comparator.comparing(Person::calcAge).thenComparing((Person::getSurname)).thenComparing(Person::getMass)
        );
        people.addAll(peopleAgeAsc);
        people.add(new Person("Stef", "Vanoppen", Gender.MALE,
                LocalDate.of(1994, Month.NOVEMBER, 23),
                75, 190));
        people.stream()
                .forEach(System.out::println);
    }

    private static void opdracht6() {
        // TODO: Werk opdracht 6 af
    }

    private static void opdracht5() {
        TextUtil.printTitle("Opdracht 5");
        Queue<BurgerOrder> burgerOrders = new LinkedList<>();
        burgerOrders.add(new BurgerOrder("LargeMac", 2));
        burgerOrders.add(new BurgerOrder("MacNuggets (6 piece)", 1));
        burgerOrders.add(new BurgerOrder("MacFlurry", 4));
        while (burgerOrders.size() > 0) {
            System.out.printf("Handling order: %s%n", burgerOrders.poll());
        }
        Queue<BurgerOrder> burgerOrdersAsPriorityQueue = new PriorityQueue<>();
        burgerOrdersAsPriorityQueue.add(new BurgerOrder("LargeMac", 2));
        burgerOrdersAsPriorityQueue.add(new BurgerOrder("MacNuggets (6 piece)", 1));
        burgerOrdersAsPriorityQueue.add(new BurgerOrder("MacFlurry", 4));
        while (burgerOrdersAsPriorityQueue.size() > 0) {
            System.out.printf("Handling order: %s%n", burgerOrders.poll());
        }


    }

    private static void opdracht4() {
        TextUtil.printTitle("Opdracht 4");
        TextUtil.printSubheading("Ask user to enter words and add to array");
        SortedSet<String> inputWords = new TreeSet<>(String::compareToIgnoreCase);
        for (int i = 0; i < 5; i++) {
            inputWords.add(KeyboardUtility.ask(String.format("Enter word #%d: ", i + 1)));
        }
        inputWords.stream()
                .forEach(System.out::println);

        TextUtil.printSubheading("Printing the (alphabetically) first word in the words TreeSet");
        System.out.println(inputWords.first());

        TextUtil.printSubheading("Printing the (alphabetically) last word in the words TreeSet");
        System.out.println(inputWords.last());

    }

    private static void opdracht3() {
        TextUtil.printTitle("Opdracht 3 (LinkedHashSet instead of HashSet)");
        TextUtil.printSubheading("Ask user to enter values and add to LinkedHashSet");
        Set<Integer> inputNumberList = new LinkedHashSet<>();
        for (int i = 0; i < 5; i++) {
            inputNumberList.add(KeyboardUtility.askForInt("Enter a number"));
        }
        System.out.printf("You entered the numbers: %s%n", inputNumberList);

        TextUtil.printSubheading("Lottery program");
        Set<Integer> userLotteryNumbers = new HashSet<>();
        while (userLotteryNumbers.size() < 6) {
            userLotteryNumbers.add(KeyboardUtility.askForInt("Enter a lottery number"));
        }
        Set<Integer> randomLotteryNumbers = new HashSet<>();
        while (randomLotteryNumbers.size() < 6) {
            randomLotteryNumbers.add(RandomUtility.RANDOM_NUM_GEN.nextInt(45) + 1);
        }
        userLotteryNumbers.retainAll(randomLotteryNumbers);
        System.out.printf("You had %d correct lottery numbers: %s%n", userLotteryNumbers.size(), userLotteryNumbers);

        TextUtil.printSubheading("Person HashSet");
        Set<Person> people = new LinkedHashSet<>();
        people.add(new Person("Yannick", "Van Ham", Gender.MALE,
                LocalDate.of(1994, Month.NOVEMBER, 23),
                75, 190));
        people.add(new Person("Xavier", "Van Ham", Gender.MALE,
                LocalDate.of(1990, Month.FEBRUARY, 26),
                70, 184));
        people.add(new Person("Ludo", "Van Ham", Gender.MALE,
                LocalDate.of(1952, Month.SEPTEMBER, 11),
                80, 175));
        people.add(new Person("Martine", "Van Ham", Gender.FEMALE,
                LocalDate.of(1960, Month.AUGUST, 16),
                65, 170));
        people.add(new Person("Yannick", "Van Ham", Gender.MALE,
                LocalDate.of(1994, Month.NOVEMBER, 23),
                75, 190));
        people.stream().
                forEach(System.out::println);

    }

    private static void opdracht2() {
        TextUtil.printTitle("Opdracht 2");
        TextUtil.printSubheading("Ask user to enter values and add to HashSet");
        Set<Integer> inputNumberList = new HashSet<>();
        for (int i = 0; i < 5; i++) {
            inputNumberList.add(KeyboardUtility.askForInt("Enter a number"));
        }
        System.out.printf("You entered the numbers: %s%n", inputNumberList);

        TextUtil.printSubheading("Lottery program");
        Set<Integer> userLotteryNumbers = new HashSet<>();
        while (userLotteryNumbers.size() < 6) {
            userLotteryNumbers.add(KeyboardUtility.askForInt("Enter a lottery number"));
        }
        Set<Integer> randomLotteryNumbers = new HashSet<>();
        while (randomLotteryNumbers.size() < 6) {
            randomLotteryNumbers.add(RandomUtility.RANDOM_NUM_GEN.nextInt(45) + 1);
        }
        userLotteryNumbers.retainAll(randomLotteryNumbers);
        System.out.printf("You had %d correct lottery numbers: %s%n", userLotteryNumbers.size(), userLotteryNumbers);

        TextUtil.printSubheading("Person HashSet");
        Set<Person> people = new HashSet<>();
        people.add(new Person("Yannick", "Van Ham", Gender.MALE,
                LocalDate.of(1994, Month.NOVEMBER, 23),
                75, 190));
        people.add(new Person("Xavier", "Van Ham", Gender.MALE,
                LocalDate.of(1990, Month.FEBRUARY, 26),
                70, 184));
        people.add(new Person("Ludo", "Van Ham", Gender.MALE,
                LocalDate.of(1952, Month.SEPTEMBER, 11),
                80, 175));
        people.add(new Person("Martine", "Van Ham", Gender.FEMALE,
                LocalDate.of(1960, Month.AUGUST, 16),
                65, 170));
        people.add(new Person("Yannick", "Van Ham", Gender.MALE,
                LocalDate.of(1994, Month.NOVEMBER, 23),
                75, 190));
        people.stream().
                forEach(System.out::println);

    }

    private static void opdracht1() {
        TextUtil.printTitle("Opdracht 1");
        TextUtil.printSubheading("Ask user to enter values and add to array");
        List<Integer> inputNumberList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            inputNumberList.add(KeyboardUtility.askForInt("Enter a number"));
        }
        System.out.printf("You entered the numbers: %s%n", inputNumberList);

        TextUtil.printSubheading("Sum of entered numbers");
        System.out.println(
                inputNumberList.stream()
                        .mapToInt(Integer::intValue)
                        .sum());

        TextUtil.printSubheading("Average of entered numbers");
        System.out.println(
                inputNumberList.stream()
                        .mapToInt(Integer::intValue)
                        .average().getAsDouble());

        TextUtil.printSubheading("Ask user to enter words and add to array");
        List<String> inputWords = new ArrayList<>();
        boolean isFinalWord = false;
        while (!isFinalWord) {
            String inputWord = KeyboardUtility.ask("Enter a word: ");
            inputWords.add(inputWord);
            if (inputWord.charAt(inputWord.length() - 1) == '.') {
                isFinalWord = true;
            }
        }
        for (int i = inputWords.size() - 1; i >= 0; i--) {
            String word = inputWords.get(i);
            StringBuilder wordAsSB = new StringBuilder(word).reverse(); // Using StringBuilder as String lacks reverse() method
            System.out.print(wordAsSB.toString() + " ");
        }
        System.out.println();

        TextUtil.printSubheading("Map List to Array and print on screen");
        String[] words = (String[]) inputWords.toArray();
        for (String word : words) {
            System.out.println(word);
        }
    }

}
