package be.moon.hs20.book.yannick;

import be.moon.hs19.opdrachten.Gender;
import be.moon.hs19.opdrachten.Person;


import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created By Moon
 * 2/9/2021, Tue
 **/
public class ArraysDemo {
    public static void main(String[] args) {
        String[] guestList = {
                "Michael Shannon",
                "Henry Witherton",
                "Sara Sherwood",
                "Michaela Stone"};
        String weddingCrasher = "Jonathan Barnes";

        // Declare new (temporary) array with original array's length + 1
        String[] newArray = new String[guestList.length + 1];

        // Copy over all the contents
        for (int i = 0; i < guestList.length; i++) {
            newArray[i] = guestList[i];
        }

        newArray[guestList.length] = weddingCrasher;

        guestList = newArray;
        //Stream.of(guestList).forEach(System.out::println);

        // New and improved ArrayList
        ArrayList<String> guestListImproved = new ArrayList<>();
        guestListImproved.add("Michael Shannon");
        guestListImproved.add("Henry Witherton");
        guestListImproved.add("Sara Sherwood");
        guestListImproved.add("Michaela Stone");
        guestListImproved.add("Michaela Stone");

//        System.out.println(guestListImproved);

        // ================

        Integer[] primeNumbers = new Integer[4];
//        System.out.printf("Array length is: %d%n", primeNumbers.length);

        ArrayList<Integer> primeNumbersAsArrayList = new ArrayList<>();
        primeNumbersAsArrayList.add(2);
        primeNumbersAsArrayList.add(3);
        primeNumbersAsArrayList.add(4);
        primeNumbersAsArrayList.add(7);
        primeNumbersAsArrayList.add(11);
        primeNumbersAsArrayList.add(13);
        primeNumbersAsArrayList.add(17);
        primeNumbersAsArrayList.add(19);
        primeNumbersAsArrayList.add(23);
        primeNumbersAsArrayList.add(27); // 10
        primeNumbersAsArrayList.add(31);
        System.out.printf("ArrayList size is: %d%n", primeNumbersAsArrayList.size());

//        System.out.println(primeNumbersAsArrayList.get(203));

        ArrayList<Person> peopleAsArrayList = new ArrayList<>();
        List<Person> sortedPeople = peopleAsArrayList.stream()
                .filter(p -> p.calculateAge() > 18)
                .sorted((p1, p2) -> p1.getSurname().compareTo(p2.getSurname()))
                .collect(Collectors.toList());


        TreeSet<Person> people = new TreeSet<>(
                (p1, p2) -> {
                    return p1.getSurname().compareTo(p2.getSurname());
                }
        );

        people.add(new Person("Xavier", "Van Ham", Gender.MALE,  LocalDate.of(1990, Month.FEBRUARY, 26), 0, 0));
        people.add(new Person("Asya", "Eds", Gender.FEMALE,  LocalDate.of(1994, Month.NOVEMBER, 23),0, 0));
        people.add(new Person("Mars", "Gang", Gender.MALE,  LocalDate.of(1952, Month.SEPTEMBER, 11),0, 0));




    }

    private static void tryToAddToArray(String[] array, String elementToAdd) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                array[i] = elementToAdd;
                break;
            } else {
                System.out.println("No more empty slots in the array");
            }
        }
    }

}
