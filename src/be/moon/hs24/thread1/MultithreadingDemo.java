package be.moon.hs24.thread1;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created By Moon
 * 2/16/2021, Tue
 **/
public class MultithreadingDemo {
    public static void main(String[] args) {
        Thread thread1 = new Thread(() ->{
            Collection<Integer> primeNumbers = populateColWithPrimeNums(10_000);

            primeNumbers.forEach(i -> System.out.printf("%,d%n" , i));
        });


        String text = "Om de compiler op de hoogte te brengen van waar hij andere gecompileerde klassen kan vinden gebruiken we het classpath. " +
                "Het classpath is een opsomming van paden waarin de compiler – maar ook andere toepassingen – " +
                "op zoek kunnen gaan naar klassen die nodig zijn voor de uitvoering. In ons geval is de map bin een map waarin zich klassenbestanden " +
                "(bytecode) bevinden. We kunnen met de optie -classpath deze map toevoegen en de compiler op de hoogte brengen van waar hij moet zoeken.";
        Thread thread2 = new Thread(() ->{
            printCharOfText(text);
        });

        System.out.println(thread1.getState());

        thread1.start();
        thread2.start();





    }

    private static void printCharOfText(String text) {
        for (int i = 0; i < text.length(); i++) {
            System.out.println(text.charAt(i));
        }
    }

    private static Collection<Integer> populateColWithPrimeNums(int upperlimit) {
        Collection<Integer> col = new HashSet<>();
        for (int j = 2; j < upperlimit; j++) {
            if(isPrime(j)){
                col.add(j);
            }
        }
        return col;
    }

    private static boolean isPrime(int number) {
        int i, m=0, flag=0;
        m=number/2;
            for(i=2;i<=m;i++){
                if(number % i==0){
                    flag=1;
                    return false;

                }
            }
        return true;
    }
}
