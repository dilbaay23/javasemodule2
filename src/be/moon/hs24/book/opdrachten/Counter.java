package be.moon.hs24.book.opdrachten;

/**
 * Created By Moon
 * 2/17/2021, Wed
 **/
public class Counter {

    private int count;

    public Counter() {
        this.count = 0;
    }

    public synchronized void increment() {
         count++;
    }

    public synchronized void decrement() {
         count--;
    }

    public int getCount() {
        return count;
    }

}
