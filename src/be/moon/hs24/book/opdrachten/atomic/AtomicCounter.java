package be.moon.hs24.book.opdrachten.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created By Moon
 * 2/17/2021, Wed
 **/
public class AtomicCounter {

    private AtomicInteger count;

    public AtomicCounter() {
        this.count= new AtomicInteger(0);
    }

    public synchronized void increment() {

        count.getAndIncrement();
    }

    public synchronized void decrement() {

        count.getAndDecrement();
    }

    public int getCount() {
        return count.get();
    }

}
