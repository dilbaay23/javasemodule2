package be.moon.hs24.book.opdrachten.atomic;

import be.moon.hs24.book.opdrachten.Counter;

/**
 * Created By Moon
 * 2/17/2021, Wed
 **/
public class AtomicApp {
    public static void main(String[] args) {
        AtomicCounter atomicCounter =new AtomicCounter();

        int amount= 1_000;

        Thread thread1 = new Thread(() -> incrementNTimes(atomicCounter,amount));
        Thread thread2 = new Thread(() -> incrementNTimes(atomicCounter,amount));
        Thread thread3 = new Thread(() -> decrementNTimes(atomicCounter,amount));

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        System.out.println(atomicCounter.getCount());
    }

    private static void  incrementNTimes(AtomicCounter atomicCounter,int incrementAmount){
        for (int i = 0; i < incrementAmount; i++) {
            atomicCounter.increment();
        }
    }
    private static void  decrementNTimes(AtomicCounter atomicCounter,int decrementAmount){
        for (int i = 0; i < decrementAmount; i++) {
            atomicCounter.decrement();
        }
    }

}
