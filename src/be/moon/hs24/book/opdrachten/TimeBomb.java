package be.moon.hs24.book.opdrachten;

import java.awt.*;

/**
 * Created By Moon
 * 2/16/2021, Tue
 **/
public class TimeBomb implements Runnable{

    private int time;
    private Thread thread;

    public TimeBomb(int seconds) {
        this.time = seconds;
    }

    public int getTime() {
        return time;
    }

    public void  activate(){
        this.thread = new Thread(this);
        this.thread.start();
    }

    @Override
    public void run() {
        try {
            for (int i = time; i >= 0; i--) {
                System.out.printf("Timebomb ticking: %d seconds left%n", i);
                Toolkit.getDefaultToolkit().beep();
                Thread.sleep(1_000);
            }
            explode();
        } catch (InterruptedException e) {
            System.out.println("Disarmed!");
        }

    }

    public void disarm(){

        this.thread.interrupt();

    }

    public void explode(){

        System.out.println("Boooooooom!");
    }
}
