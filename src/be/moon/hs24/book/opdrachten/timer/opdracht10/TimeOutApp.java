package be.moon.hs24.book.opdrachten.timer.opdracht10;

import java.time.LocalDate;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created By Moon
 * 2/17/2021, Wed
 **/
public class TimeOutApp {
    public static void main(String[] args) {

        TimerTask task = new TimeOut();
        Timer timer = new Timer(true);

        timer.scheduleAtFixedRate(task, new Date(),1_000);

        try {
            Thread.sleep(10_000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

    }
}
