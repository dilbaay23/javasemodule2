package be.moon.hs24.book.opdrachten.timer;

import java.util.TimerTask;

/**
 * Created By Moon
 * 2/17/2021, Wed
 **/
public class TimeOut extends TimerTask {
    @Override
    public void run() {
        System.out.println("Time has run out");
    }
}
