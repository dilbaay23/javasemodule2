package be.moon.hs24.book.opdrachten.timer;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created By Moon
 * 2/17/2021, Wed
 **/
public class TimerApp {
    public static void main(String[] args) {

        TimerTask timerTask =new TimeOut();
        Timer timer = new Timer(true);

        timer.schedule(timerTask,10_000);

        System.out.println("Patience please");

        try {
            Thread.sleep(20_000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}
