package be.moon.hs24.book.opdrachten;

/**
 * Created By Moon
 * 2/17/2021, Wed
 **/
public class SynchronizeApp {
    public static void main(String[] args) {
        Counter counter = new Counter();
        int amount= 1_000;
        Thread thread1 = new Thread(() -> incrementNTimes(counter,amount));
        Thread thread2 = new Thread(() -> incrementNTimes(counter,amount));
        Thread thread3 = new Thread(() -> decrementNTimes(counter,amount));

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        System.out.println(counter.getCount());
    }

    private static void  incrementNTimes(Counter counter,int incrementAmount){
        for (int i = 0; i < incrementAmount; i++) {
            counter.increment();
        }
    }
    private static void  decrementNTimes(Counter counter,int decrementAmount){
        for (int i = 0; i < decrementAmount; i++) {
            counter.decrement();
        }
    }

}
