package be.moon.hs24.book.opdrachten.parallelisme;

import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * Created By Moon
 * 2/17/2021, Wed
 **/
public class ParallelStreamApp {
    public static void main(String[] args) {
        double avg = IntStream.range(1, 90_000)
                .parallel()
                .mapToDouble(Math::sqrt)
                .map(Math::log)
                .average().getAsDouble();

        System.out.println(avg);

        long sumOfPrimes = LongStream.range(2, 5_000)
                .parallel()
                .filter(i -> isPrime((int) i))
                .sum();

        System.out.println(sumOfPrimes);

    }

    private static boolean isPrime(int number) {
        int m = number / 2;
        for (int i = 2; i <= m; i++) {
            if (number % i == 0) {
                return false;

            }
        }
        return true;
    }


}
