package be.moon.hs24.book.opdrachten.concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static be.moon.util.RandomUtility.RANDOM_NUM_GEN;

/**
 * Created By Moon
 * 2/17/2021, Wed
 **/
public class ConcurrencyApp {
    public final static int AMOUNT = 1000;

    public static void main(String[] args) {

        List<Integer> numbers = new ArrayList<>();
        List<Integer> syncNumbers = Collections.synchronizedList(numbers);  // It is thread-safe

/*        Thread thread1 = new Thread(() -> populateList(numbers));   //this is not thread-safe , so list doesn't full with 2000 elements. thread1 and thread2 try to put some elements in same index
        Thread thread2 = new Thread(() -> populateList(numbers));*/


        Thread thread1 = new Thread(() -> populateList(syncNumbers));
        Thread thread2 = new Thread(() -> populateList(syncNumbers));

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread1.join();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        System.out.println(numbers);
        System.out.println(syncNumbers);
        System.out.println(numbers.size());
        System.out.println(syncNumbers.size());

    }

    private static void populateList(List<Integer> list, int amount) {
        for (int i = 0; i < amount; i++) {
            list.add(RANDOM_NUM_GEN.nextInt(1000));
        }
    }

    private static void populateList(List<Integer> list) {
        populateList(list, AMOUNT);
    }

}
