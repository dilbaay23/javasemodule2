package be.moon.hs24.book.opdrachten;

import java.awt.*;

/**
 * Created By Moon
 * 2/16/2021, Tue
 **/
public class MultithreadingApp {
    private static final int AMOUNT =100;
    public static void main(String[] args) {

//        opdracht1();
//        opdracht2a();
        opdracht2b();
//        opdracht3();
//        opdracht4();
//        opdracht5a();
//       opdracht5b();
//        opdracht6();

    }

    private static void opdracht6() {
        Thread thread1= new Thread( ()-> printCharNTimes('*') );

        Thread thread2= new Thread(() -> printCharNTimes('#'));

        thread1.start();
        thread2.start();
        System.out.println("Not quite the end.");

        try {
            thread1.join();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        System.out.println("The End");


    }

    private static void opdracht5b() {
        try {
            Thread thread1 = new Thread(() -> printCharNTimes5b('*'));

            Thread thread2 = new Thread(() -> printCharNTimes5b('#'));

            thread1.setName("Thread1");
            thread2.setName("Thread2");



            thread1.start();
            thread2.start();

            Thread.sleep(470);
            thread1.interrupt();
            Thread.sleep(130);
            thread2.interrupt();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    private static void printCharNTimes5b(char c,int amount) {
        for (int i = 0; i < amount ; i++) {
            System.out.print(c);
            Toolkit.getDefaultToolkit().beep();
            //  Thread.yield();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.print(Thread.currentThread().getName() + " : Interrupted ");
            }
        }

    }
    private static void printCharNTimes5b(char c) {
        printCharNTimes5b(c,AMOUNT);
    }

    private static void opdracht5a() {
        System.out.print("Welcome");
        try {
            Thread.sleep(250);
            System.out.print(".");
            Thread.sleep(250);
            System.out.print(".");
            Thread.sleep(250);
            System.out.print(".");
            Thread.sleep(250);
            System.out.print(".");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void opdracht4() {
        Thread thread1= new Thread( ()-> printCharNTimes('*') );

        Thread thread2= new Thread(()->{
            printNumbersForThread(1_000_000);
        });

        thread2.setDaemon(true);  // setDaemon(true): thread2 is background thread. It will stop when thread1 finishes.

        thread1.start();
        thread2.start();
      //  System.exit(0);
    }

    private static void printNumbersForThread(int upperLimit) {
        for (int j = 0; j < upperLimit ; j++) {
            System.out.println(j);
        }
    }

    private static void opdracht3() {
        Thread thread1= new Thread( ()-> printCharNTimes('*') );

        Thread thread2= new Thread(()->{
            printCharNTimes('#');
        });
        thread1.setPriority(10);

        thread1.start();
        thread2.start();

    }

    private static void opdracht2b() {

        Thread thread1= new Thread( ()-> printCharNTimes('*') );

        Thread thread2= new Thread(()->{
            printCharNTimes('#');
        });
        System.out.println(thread1.getState());
        System.out.println(thread2.getState());

        Thread mainThread = Thread.currentThread();

        thread1.start();
        thread2.start();
        printCharNTimes('@');
        System.out.println(thread1.getState());
        System.out.println(thread2.getState());
        System.out.println(mainThread.getState());
    }

    private static void printCharNTimes(char c,int amount) {
        for (int i = 0; i < amount ; i++) {
            System.out.print(c);
            Thread.yield();
        }

    }
    private static void printCharNTimes(char c) {
        printCharNTimes(c,AMOUNT);
    }

    private static void opdracht2a() {
        CharacterPrinter characterPrinter1 = new CharacterPrinter('*',AMOUNT);
        CharacterPrinter characterPrinter2 = new CharacterPrinter('#',AMOUNT);

        Thread thread1= new Thread(characterPrinter1);
        Thread thread2= new Thread(characterPrinter2);

        thread1.start();
        thread2.start();

    }

    private static void opdracht1() {
        PrintThread printThread1 = new PrintThread('*',AMOUNT);
        PrintThread printThread2 = new PrintThread('#',AMOUNT);
        printThread1.start();
        printThread2.start();
    }
}
