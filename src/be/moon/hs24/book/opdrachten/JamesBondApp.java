package be.moon.hs24.book.opdrachten;

import java.util.Random;

/**
 * Created By Moon
 * 2/16/2021, Tue
 **/
public class JamesBondApp {
    public static void main(String[] args) {
        Random random = new Random();
        TimeBomb bomb = new TimeBomb(10);
        bomb.activate();
        try {
            Thread.sleep((random.nextInt(30_000)));
        }catch (InterruptedException exception){
            exception.printStackTrace();
        }
        bomb.disarm();
    }

}
